# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'edela' # Edit here
set :repo_url, 'git@bitbucket.org:espass/core.git' # Edit here
set :rbenv_ruby, '2.4.0'
set :rbenv_path, '/home/edela/.rbenv' # Edit here
set :deploy_to, '/home/edela/core' # Edit here
set :scm, :git
set :puma_pid, "#{shared_path}/pids/puma.pid"
set :puma_bind, "unix://#{shared_path}/sockets/puma.sock"
set :puma_state, "#{shared_path}/pids/puma.state"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, false
set :conditionally_migrate, true
set :whenever_roles, [:cron_job]
# set :pty, true

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/application.yml', 'config/shoryuken.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'pids', 'tmp/cache', 'sockets', 'vendor/bundle')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

set :keep_releases, 5
