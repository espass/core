Rails.application.routes.draw do
  namespace :api, as: '' do
    resources :users, only: [] do
      collection do
        # Authenticate
        post 'sign_in', to: 'users#sign_in'
        post 'auth/facebook', to: 'users#auth_facebook'
        post 'sign_up', to: 'users#sign_up'
        post 'sign_out', to: 'users#sign_out'
        post 'update_mobile', to: 'users#update_mobile'
        post 'verify_mobile', to: 'users#verify_mobile'
        post 'favorite_partner', to: 'users#favorite_partner'
        get 'ping', to: 'users#ping'
        get 'my_partners', to: 'users#my_partners'
        post 'forgot_password', to: 'users#forgot_password'
        post 'reset_password', to: 'users#reset_password'
      end

      member do
        get '/profile', to: 'users#profile', as: 'profile'
        post '/profile', to: 'users#update_profile', as: 'update_profile'
      end
    end

    resources :partners, only: [] do
      collection do
        get '', to: 'partners#index', as: 'filter'
        get 'search', to: 'partners#search', as: 'search'
        get 'nearby', to: 'partners#nearby'
        # Authenticate
        post 'sign_in', to: 'partners#sign_in'
        post 'sign_out', to: 'partners#sign_out'
        get 'ping', to: 'partners#ping'
      end

      member do
        get '/profile', to: 'partners#profile', as: 'profile'
        post '/profile', to: 'partners#update_profile', as: 'update_profile'
        post '/services', to: 'partners#update_services', as: 'update_services'

        get '/working_times', to: 'partners/working_times#index', as: 'list_working_times'
        get '/working_times/today', to: 'partners/working_times#today', as: 'today_working_times'
        post '/working_times', to: 'partners/working_times#create', as: 'create_working_time'
        post '/working_times/:wt_id/update', to: 'partners/working_times#update', as: 'update_working_time'
        post '/working_times/:wt_id/delete', to: 'partners/working_times#delete', as: 'delete_working_time'

        get '/albums', to: 'partners/albums#index', as: 'list_albums'
        post '/albums', to: 'partners/albums#create', as: 'create_album'
        post '/albums/:ab_id/delete', to: 'partners/albums#delete', as: 'delete_album'

        get '/albums/:ab_id/photos', to: 'partners/albums#photos', as: 'album_photos'
        post '/albums/:ab_id/add_photos', to: 'partners/albums#add_photos', as: 'add_album_photos'
        post '/albums/:ab_id/delete_photos', to: 'partners/albums#delete_photos', as: 'delete_album_photos'

        get '/reviews', to: 'partners/reviews#index', as: 'list_reviews'
        post '/reviews', to: 'partners/reviews#create', as: 'create_review'

        post '/cover_photos', to: 'partners/cover_photos#create', as: 'create_cover_photo'
      end
    end

    resources :services, only: [] do
      collection do
        get '', to: 'services#index', as: 'list'
      end
    end

    resources :orders, only: [] do
      collection do
        get '', to: 'orders#index', as: 'list'
        get 'coming-soon', to: 'orders#coming_soon', as: 'coming_soon'
        get 'history', to: 'orders#history', as: 'history'
        get 'scheduled', to: 'orders#scheduled', as: 'scheduled'
        post '', to: 'orders#create', as: 'create'
        post 'create_quickly', to: 'orders#create_quickly', as: 'create_quickly'
      end

      member do
        get 'detail', to: 'orders#detail', as: 'detail'
        post 'accept', to: 'orders#accept', as: 'accept'
        post 'decline', to: 'orders#decline', as: 'decline'
        post 'cancel', to: 'orders#cancel', as: 'cancel'
        post 'finish', to: 'orders#finish', as: 'finish'
        post 'process', to: 'orders#partner_process', as: 'process'
      end
    end

    resources :devices, only: [] do
      collection do
        post '', to: 'devices#register', as: 'register'
      end
    end

    resources :libraries, only: [] do
      collection do
        post 'create', to: 'libraries#create', as: 'create'
      end

      member do
        post 'delete', to: 'libraries#delete', as: 'delete'
      end
    end

    resources :notifications, only: [] do
      collection do
        get '', to: 'notifications#index', as: 'list'
      end

      member do
        post 'delete', to: 'notifications#delete', as: 'delete'
        post 'mark_read', to: 'notifications#mark_read', as: 'mark_read'
      end
    end

    resources :settings, only: [] do
      collection do
        get 'price_list', to: 'settings#price_list', as: 'price_list'
        get '', to: 'settings#index', as: 'application'
      end
    end

    resources :issues, only: [] do
      collection do
        post '', to: 'issues#create', as: 'create'
      end
    end

    resources :coupons, only: [] do
      collection do
        post 'check', to: 'coupons#check', as: 'check'
      end
    end

    resources :reports, only: [] do
      collection do
        get 'earning', to: 'reports#earning', as: 'earning'
      end
    end
  end
end
