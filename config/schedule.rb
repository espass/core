# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

env :PATH, ENV['PATH']
env :GEM_PATH, ENV['GEM_PATH']

set :output, 'log/cron_log.log'
set :environment, ENV['RAILS_ENV'] || 'development'

every 1.minute, roles: [:cron_job] do
  rake 'find_new_partner:go'
end
