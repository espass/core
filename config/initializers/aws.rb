credentials = Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'])
Aws.config[:region] = ENV['AWS_REGION']
Aws.config[:credentials] = credentials

S3 = Aws::S3::Resource.new
MEDIA_BUCKET = S3.bucket(ENV['AWS_S3_MEDIA_BUCKET'])
