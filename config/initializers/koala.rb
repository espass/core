Koala.configure do |config|
  config.app_id = ENV['FB_ACCESS']
  config.app_secret = ENV['FB_SECRET']
end
