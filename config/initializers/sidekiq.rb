require 'sidekiq'

Sidekiq.configure_client do |config|
  config.redis = { url: "redis://#{ENV['REDIS_HOST']}:6379/12", namespace: 'worker', network_timeout: 5 }
  config.average_scheduled_poll_interval = 25
end

unless Rails.env.production?
  Sidekiq.configure_server do |config|
    config.redis = { url: "redis://#{ENV['REDIS_HOST']}:6379/12", namespace: 'worker', network_timeout: 5 }
    config.average_scheduled_poll_interval = 25
  end
end
