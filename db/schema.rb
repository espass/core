# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170804045101) do

  create_table "coupons", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "type"
    t.string   "code"
    t.string   "description"
    t.datetime "expiry_at"
    t.float    "discount",               limit: 24
    t.string   "discount_type"
    t.integer  "usage_limit_per_coupon"
    t.integer  "usage_limit_per_user"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["code"], name: "index_coupons_on_code", using: :btree
  end

  create_table "devices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "os"
    t.string   "os_version"
    t.string   "lang"
    t.string   "device_id"
    t.string   "device_token"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["device_id"], name: "index_devices_on_device_id", unique: true, using: :btree
  end

  create_table "identities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uid"
    t.string   "provider"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_identities_on_user_id", using: :btree
  end

  create_table "issues", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "issue_type"
    t.string   "title"
    t.string   "description"
    t.integer  "user_id"
    t.integer  "partner_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "libraries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "key"
    t.string   "url"
    t.string   "media_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "partner_id"
    t.boolean  "is_deleted"
    t.index ["partner_id"], name: "index_libraries_on_partner_id", using: :btree
    t.index ["user_id"], name: "index_libraries_on_user_id", using: :btree
  end

  create_table "notifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "target_type"
    t.string   "target"
    t.string   "title"
    t.string   "thumbnail"
    t.integer  "recipient"
    t.string   "recipient_name"
    t.boolean  "is_deleted"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["is_deleted"], name: "index_notifications_on_is_deleted", using: :btree
    t.index ["recipient"], name: "index_notifications_on_recipient", using: :btree
    t.index ["recipient_name"], name: "index_notifications_on_recipient_name", using: :btree
  end

  create_table "order_booking_lines", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "status"
    t.datetime "finished_at"
    t.datetime "started_at"
    t.integer  "num_partner"
    t.datetime "expiry_at"
    t.integer  "assigned_to"
    t.integer  "request_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "order_id"
    t.index ["order_id"], name: "index_order_booking_lines_on_order_id", using: :btree
  end

  create_table "order_level_histories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "change_data"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "order_id"
    t.index ["order_id"], name: "index_order_level_histories_on_order_id", using: :btree
  end

  create_table "order_request_histories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "status"
    t.string   "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "partner_id"
    t.integer  "order_id"
    t.index ["order_id"], name: "index_order_request_histories_on_order_id", using: :btree
    t.index ["partner_id"], name: "index_order_request_histories_on_partner_id", using: :btree
  end

  create_table "order_services", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "order_id"
    t.integer  "price"
    t.integer  "partner_service_id"
    t.index ["order_id"], name: "index_order_services_on_order_id", using: :btree
    t.index ["partner_service_id"], name: "index_order_services_on_partner_service_id", using: :btree
  end

  create_table "orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.datetime "booking_time"
    t.string   "location"
    t.string   "address"
    t.integer  "price"
    t.string   "status"
    t.integer  "user_id"
    t.integer  "partner_id"
    t.string   "booking_type"
    t.text     "attachments",     limit: 65535
    t.string   "base_services"
    t.string   "level"
    t.string   "coupon_code"
    t.float    "coupon_discount", limit: 24
    t.datetime "finished_at"
    t.index ["booking_time"], name: "index_orders_on_booking_time", using: :btree
    t.index ["partner_id"], name: "index_orders_on_partner_id", using: :btree
    t.index ["user_id"], name: "index_orders_on_user_id", using: :btree
  end

  create_table "partner_albums", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "title"
    t.string   "thumbnail"
    t.integer  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "partner_id"
    t.boolean  "is_deleted"
    t.integer  "num_photo"
    t.string   "description"
    t.index ["is_deleted"], name: "index_partner_albums_on_is_deleted", using: :btree
    t.index ["partner_id"], name: "index_partner_albums_on_partner_id", using: :btree
  end

  create_table "partner_cover_photos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "title"
    t.string   "link"
    t.boolean  "is_deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "partner_id"
    t.index ["is_deleted"], name: "index_partner_cover_photos_on_is_deleted", using: :btree
    t.index ["partner_id"], name: "index_partner_cover_photos_on_partner_id", using: :btree
  end

  create_table "partner_devices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "device_id"
    t.index ["device_id"], name: "index_partner_devices_on_device_id", using: :btree
    t.index ["user_id"], name: "index_partner_devices_on_user_id", using: :btree
  end

  create_table "partner_photos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "title"
    t.string   "link"
    t.integer  "status"
    t.integer  "album_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "partner_id"
    t.boolean  "featured"
    t.boolean  "is_deleted"
    t.index ["album_id"], name: "index_partner_photos_on_album_id", using: :btree
    t.index ["featured"], name: "index_partner_photos_on_featured", using: :btree
    t.index ["is_deleted"], name: "index_partner_photos_on_is_deleted", using: :btree
    t.index ["partner_id"], name: "index_partner_photos_on_partner_id", using: :btree
  end

  create_table "partner_profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "name"
    t.text     "address",                 limit: 65535
    t.string   "location"
    t.text     "avatar",                  limit: 65535
    t.integer  "partner_id"
    t.text     "description",             limit: 65535
    t.string   "mobile"
    t.float    "rating",                  limit: 24
    t.integer  "num_rate"
    t.float    "fake_rating",             limit: 24
    t.integer  "fake_num_rate"
    t.boolean  "fake_enabled"
    t.integer  "min_price"
    t.integer  "max_price"
    t.integer  "num_album"
    t.integer  "year_exp"
    t.integer  "num_unread_notification"
    t.text     "unread_notifications",    limit: 65535
    t.index ["partner_id"], name: "index_partner_profiles_on_partner_id", using: :btree
  end

  create_table "partner_rating_reports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "one"
    t.integer  "two"
    t.integer  "three"
    t.integer  "four"
    t.integer  "five"
    t.integer  "num_rate"
    t.float    "rating",     limit: 24
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "partner_id"
    t.index ["partner_id"], name: "index_partner_rating_reports_on_partner_id", using: :btree
  end

  create_table "partner_reviews", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "title"
    t.text     "description", limit: 65535
    t.float    "rating",      limit: 24
    t.boolean  "is_approved"
    t.boolean  "is_deleted"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "partner_id"
    t.integer  "user_id"
    t.integer  "order_id"
    t.index ["is_approved"], name: "index_partner_reviews_on_is_approved", using: :btree
    t.index ["is_deleted"], name: "index_partner_reviews_on_is_deleted", using: :btree
    t.index ["order_id"], name: "index_partner_reviews_on_order_id", using: :btree
    t.index ["partner_id"], name: "index_partner_reviews_on_partner_id", using: :btree
    t.index ["user_id"], name: "index_partner_reviews_on_user_id", using: :btree
  end

  create_table "partner_services", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "name"
    t.text     "description", limit: 65535
    t.integer  "price"
    t.integer  "partner_id"
    t.integer  "service_id"
    t.text     "thumbnail",   limit: 65535
    t.index ["partner_id"], name: "index_partner_services_on_partner_id", using: :btree
    t.index ["service_id"], name: "index_partner_services_on_service_id", using: :btree
  end

  create_table "partners", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "email"
    t.integer  "status",          limit: 1
    t.string   "auth_token"
    t.datetime "token_expiry"
    t.datetime "last_sign_in_at"
    t.string   "last_sign_in_ip"
    t.string   "password_digest"
    t.integer  "device_id"
    t.index ["email"], name: "index_partners_on_email", unique: true, using: :btree
  end

  create_table "services", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "name"
    t.text     "description", limit: 65535
    t.text     "thumbnail",   limit: 65535
    t.integer  "min_price"
    t.integer  "max_price"
    t.integer  "status",      limit: 1
  end

  create_table "transactions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "order_id"
    t.integer  "amount"
    t.string   "status"
    t.string   "method"
    t.index ["order_id"], name: "index_transactions_on_order_id", using: :btree
  end

  create_table "user_devices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "device_id"
    t.index ["device_id"], name: "index_user_devices_on_device_id", using: :btree
    t.index ["user_id"], name: "index_user_devices_on_user_id", using: :btree
  end

  create_table "user_favorites", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "partner_id"
    t.integer  "user_id"
    t.index ["partner_id"], name: "index_user_favorites_on_partner_id", using: :btree
    t.index ["user_id"], name: "index_user_favorites_on_user_id", using: :btree
  end

  create_table "user_profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "name"
    t.string   "address"
    t.text     "description",             limit: 65535
    t.text     "avatar",                  limit: 65535
    t.text     "location",                limit: 65535
    t.integer  "user_id"
    t.string   "mobile"
    t.boolean  "is_verified"
    t.integer  "pin"
    t.datetime "pin_sent_at"
    t.string   "cover_photo"
    t.string   "reset_password_token"
    t.datetime "reset_password_expiry"
    t.integer  "num_unread_notification"
    t.text     "unread_notifications",    limit: 65535
    t.index ["user_id"], name: "index_user_profiles_on_user_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "email"
    t.integer  "status",          limit: 1
    t.string   "auth_token"
    t.datetime "token_expiry"
    t.datetime "last_sign_in_at"
    t.string   "last_sign_in_ip"
    t.string   "password_digest"
    t.integer  "device_id"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  end

  create_table "working_times", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "from"
    t.string   "to"
    t.string   "repeat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "partner_id"
    t.boolean  "enabled"
    t.index ["partner_id"], name: "index_working_times_on_partner_id", using: :btree
  end

  add_foreign_key "identities", "users"
  add_foreign_key "libraries", "partners"
  add_foreign_key "libraries", "users"
  add_foreign_key "order_booking_lines", "orders"
  add_foreign_key "order_level_histories", "orders"
  add_foreign_key "order_request_histories", "orders"
  add_foreign_key "order_request_histories", "partners"
  add_foreign_key "order_services", "orders"
  add_foreign_key "order_services", "partner_services"
  add_foreign_key "orders", "partners"
  add_foreign_key "orders", "users"
  add_foreign_key "partner_albums", "partners"
  add_foreign_key "partner_cover_photos", "partners"
  add_foreign_key "partner_devices", "devices"
  add_foreign_key "partner_devices", "users"
  add_foreign_key "partner_photos", "partners"
  add_foreign_key "partner_profiles", "partners"
  add_foreign_key "partner_rating_reports", "partners"
  add_foreign_key "partner_reviews", "orders"
  add_foreign_key "partner_reviews", "partners"
  add_foreign_key "partner_reviews", "users"
  add_foreign_key "partner_services", "partners"
  add_foreign_key "partner_services", "services"
  add_foreign_key "transactions", "orders"
  add_foreign_key "user_devices", "devices"
  add_foreign_key "user_devices", "users"
  add_foreign_key "user_favorites", "partners"
  add_foreign_key "user_favorites", "users"
  add_foreign_key "user_profiles", "users"
  add_foreign_key "working_times", "partners"
end
