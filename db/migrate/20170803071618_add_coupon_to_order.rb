class AddCouponToOrder < ActiveRecord::Migration[5.0]
  def self.up
    add_column :orders, :coupon_code, :string
    add_column :orders, :coupon_discount, :float
  end

  def self.down
    remove_column :orders, :coupon_code, :string
    remove_column :orders, :coupon_discount, :float
  end
end
