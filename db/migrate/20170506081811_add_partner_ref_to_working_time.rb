class AddPartnerRefToWorkingTime < ActiveRecord::Migration[5.0]
  def change
    add_reference :working_times, :partner, foreign_key: true
  end
end
