class CreateUserDevices < ActiveRecord::Migration[5.0]
  def self.up
    create_table :user_devices do |t|
      t.timestamps
    end
  end

  def self.down
    drop_table :user_devices
  end
end
