class AddUnreadNotificationToPartnerProfile < ActiveRecord::Migration[5.0]
  def self.up
    add_column :partner_profiles, :num_unread_notification, :integer
    add_column :partner_profiles, :unread_notifications, :text
  end

  def self.down
    remove_column :partner_profiles, :num_unread_notification, :integer
    remove_column :partner_profiles, :unread_notifications, :text
  end
end
