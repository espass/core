class AddServiceRefToOrderService < ActiveRecord::Migration[5.0]
  def change
    add_reference :order_services, :service, foreign_key: true
  end
end
