class AddRatingToPartnerProfile < ActiveRecord::Migration[5.0]
  def change
  	add_column :partner_profiles, :rating, :float
  	add_column :partner_profiles, :num_rate, :integer
  	add_column :partner_profiles, :fake_rating, :float
  	add_column :partner_profiles, :fake_num_rate, :integer
  	add_column :partner_profiles, :fake_enabled, :boolean
  end
end
