class CreatePartnerRatingReports < ActiveRecord::Migration[5.0]
  def self.up
    create_table :partner_rating_reports do |t|
      t.integer :one
      t.integer :two
      t.integer :three
      t.integer :four
      t.integer :five
      t.integer :num_rate
      t.float :rating

      t.timestamps
    end

    add_reference :partner_rating_reports, :partner, foreign_key: true
  end

  def self.down
    drop_table :partner_rating_reports
  end
end
