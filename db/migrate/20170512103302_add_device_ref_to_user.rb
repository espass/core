class AddDeviceRefToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :device_id, :integer
  end
end
