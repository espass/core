class AddDeletedFlagToAlbumAndPhoto < ActiveRecord::Migration[5.0]
  def self.up
    add_column :partner_albums, :is_deleted, :boolean
    add_index :partner_albums, :is_deleted

    add_column :partner_photos, :is_deleted, :boolean
    add_index :partner_photos, :is_deleted
  end

  def self.down
    remove_column :partner_albums, :is_deleted, :boolean
    remove_column :partner_photos, :is_deleted, :boolean
  end
end
