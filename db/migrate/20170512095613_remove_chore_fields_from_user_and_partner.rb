class RemoveChoreFieldsFromUserAndPartner < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :current_sign_in_at, :datetime
    remove_column :users, :current_sign_in_ip, :string
    remove_column :partners, :current_sign_in_at, :datetime
    remove_column :partners, :current_sign_in_ip, :string
  end
end
