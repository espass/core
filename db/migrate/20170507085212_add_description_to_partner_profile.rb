class AddDescriptionToPartnerProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_profiles, :description, :text
  end
end
