class IndexDeviceIdForDevice < ActiveRecord::Migration[5.0]
  def change
    add_index :devices, :device_id, unique: true
  end
end
