class AddPriceRangeToPartnerProfile < ActiveRecord::Migration[5.0]
  def change
  	add_column :partner_profiles, :min_price, :integer
  	add_column :partner_profiles, :max_price, :integer
  end
end
