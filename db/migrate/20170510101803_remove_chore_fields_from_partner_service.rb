class RemoveChoreFieldsFromPartnerService < ActiveRecord::Migration[5.0]
  def change
    remove_column :partner_services, :mobile, :string
    remove_column :partner_services, :facebook, :string
  end
end
