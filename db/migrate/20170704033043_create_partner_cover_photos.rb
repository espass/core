class CreatePartnerCoverPhotos < ActiveRecord::Migration[5.0]
  def self.up
    create_table :partner_cover_photos do |t|
      t.string :title
      t.string :link
      t.boolean :is_deleted

      t.timestamps
    end

    add_index :partner_cover_photos, :is_deleted
    add_reference :partner_cover_photos, :partner, foreign_key: true
  end

  def self.down
    drop_table :partner_cover_photos
  end
end
