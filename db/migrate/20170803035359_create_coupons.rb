class CreateCoupons < ActiveRecord::Migration[5.0]
  def self.up
    create_table :coupons do |t|
      # general
      t.string :type
      t.string :code, index: true
      t.string :description
      t.datetime :expiry_at
      t.float :discount # depending on discount type
      t.string :discount_type

      # Usage limits
      t.integer :usage_limit_per_coupon
      t.integer :usage_limit_per_user

      t.timestamps
    end
  end

  def self.down
    drop_table :coupons
  end
end
