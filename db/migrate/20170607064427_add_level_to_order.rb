class AddLevelToOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :level, :string
  end
end
