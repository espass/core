class CreateWorkingTimes < ActiveRecord::Migration[5.0]
  def change
    create_table :working_times do |t|
      t.string :from
      t.string :to
      t.string :repeat

      t.timestamps
    end
  end
end
