class AddAuthenFieldToPartner < ActiveRecord::Migration[5.0]
  def change
    add_column :partners, :auth_token, :string
    add_column :partners, :token_expiry, :DateTime
    add_column :partners, :last_sign_in_at, :DateTime
    add_column :partners, :last_sign_in_ip, :string
    add_column :partners, :current_sign_in_at, :DateTime
    add_column :partners, :current_sign_in_ip, :string
    add_column :partners, :password_digest, :string
  end
end
