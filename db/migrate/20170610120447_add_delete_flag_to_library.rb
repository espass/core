class AddDeleteFlagToLibrary < ActiveRecord::Migration[5.0]
  def change
    add_column :libraries, :is_deleted, :boolean
  end
end
