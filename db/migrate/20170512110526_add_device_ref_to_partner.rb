class AddDeviceRefToPartner < ActiveRecord::Migration[5.0]
  def change
    add_column :partners, :device_id, :integer
  end
end
