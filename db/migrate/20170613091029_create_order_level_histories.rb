class CreateOrderLevelHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :order_level_histories do |t|
      t.string :change_data
      t.timestamps
    end

    add_reference :order_level_histories, :order, foreign_key: true
  end
end
