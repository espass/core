class CreateIssues < ActiveRecord::Migration[5.0]
  def self.up
    create_table :issues do |t|
      t.string :issue_type
      t.string :title
      t.string :description
      t.integer :user_id
      t.integer :partner_id

      t.timestamps
    end
  end

  def self.down
    drop_table :issues
  end
end
