class AddPinToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :pin, :integer
    add_column :user_profiles, :pin_sent_at, :datetime
  end
end
