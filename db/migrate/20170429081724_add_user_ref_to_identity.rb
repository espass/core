class AddUserRefToIdentity < ActiveRecord::Migration[5.0]
  def change
    add_reference :identities, :user, foreign_key: true
  end
end
