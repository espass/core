class AddDescriptionToAlbum < ActiveRecord::Migration[5.0]
  def self.up
    add_column :partner_albums, :description, :string
  end

  def self.down
    remove_column :partner_albums, :description, :string
  end
end
