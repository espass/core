class AddBaseFieldToPartnerService < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_services, :name, :string
    add_column :partner_services, :description, :text
    add_column :partner_services, :price, :int
  end
end
