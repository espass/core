class AddCoverPhotoToUserProfile < ActiveRecord::Migration[5.0]
  def self.up
    add_column :user_profiles, :cover_photo, :string
  end

  def self.down
    remove_column :user_profiles, :cover_photo, :string
  end
end
