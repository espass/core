class AddBaseFieldToPartner < ActiveRecord::Migration[5.0]
  def change
    add_column :partners, :email, :string
    add_column :partners, :status, :tinyint
    add_column :partners, :mobile, :string
    add_index :partners, :email, unique: true
  end
end
