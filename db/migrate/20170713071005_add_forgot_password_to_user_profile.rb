class AddForgotPasswordToUserProfile < ActiveRecord::Migration[5.0]
  def self.up
    add_column :user_profiles, :reset_password_token, :string
    add_column :user_profiles, :reset_password_expiry, :datetime
  end

  def self.down
    remove_column :user_profiles, :reset_password_token, :string
    remove_column :user_profiles, :reset_password_expiry, :datetime
  end
end
