class AddTypeToOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :booking_type, :string
    add_column :orders, :attachments, :text
    add_column :orders, :base_services, :string
  end
end
