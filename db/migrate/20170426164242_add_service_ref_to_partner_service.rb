class AddServiceRefToPartnerService < ActiveRecord::Migration[5.0]
  def change
    add_reference :partner_services, :service, foreign_key: true
  end
end
