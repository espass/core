class AddBaseFieldToPartnerProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_profiles, :name, :string
    add_column :partner_profiles, :address, :text
    add_column :partner_profiles, :location, :string
    add_column :partner_profiles, :avatar, :text
    add_column :partner_services, :mobile, :string
    add_column :partner_services, :facebook, :string
  end
end
