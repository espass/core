class AddAuthenFieldToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :auth_token, :string
    add_column :users, :token_expiry, :DateTime
    add_column :users, :last_sign_in_at, :DateTime
    add_column :users, :last_sign_in_ip, :string
    add_column :users, :current_sign_in_at, :DateTime
    add_column :users, :current_sign_in_ip, :string
    add_column :users, :password_digest, :string
  end
end
