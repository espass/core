class AddThumbnailToPartnerService < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_services, :thumbnail, :text
  end
end
