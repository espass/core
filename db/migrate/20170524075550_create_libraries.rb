class CreateLibraries < ActiveRecord::Migration[5.0]
  def change
    create_table :libraries do |t|
      t.string :key
      t.string :url
      t.string :media_type

      t.timestamps
    end

    add_reference :libraries, :user, foreign_key: true
    add_reference :libraries, :partner, foreign_key: true
  end
end
