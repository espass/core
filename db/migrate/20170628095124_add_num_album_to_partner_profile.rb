class AddNumAlbumToPartnerProfile < ActiveRecord::Migration[5.0]
  def self.up
    add_column :partner_profiles, :num_album, :integer
  end

  def self.down
    remove_column :partner_profiles, :num_album, :integer
  end
end
