class CreatePartnerAlbums < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_albums do |t|
      t.string :title
      t.string :thumbnail
      t.integer :status

      t.timestamps
    end

    add_reference :partner_albums, :partner, foreign_key: true
  end
end
