class AddBaseFieldToTransaction < ActiveRecord::Migration[5.0]
  def self.up
    add_column :transactions, :amount, :int
    add_column :transactions, :status, :string
    add_column :transactions, :method, :string
  end

  def self.down
    remove_column :transactions, :amount, :int
    remove_column :transactions, :status, :string
    remove_column :transactions, :method, :string
  end
end
