class AddIsVerifiedToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :is_verified, :boolean
  end
end
