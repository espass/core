class AddNumPhotoToAlbum < ActiveRecord::Migration[5.0]
  def self.up
    add_column :partner_albums, :num_photo, :integer
  end

  def self.down
    remove_column :partner_albums, :num_photo, :integer
  end
end
