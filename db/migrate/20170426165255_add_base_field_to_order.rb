class AddBaseFieldToOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :booking_time, :datetime
    add_column :orders, :location, :string
    add_column :orders, :address, :string
    add_column :orders, :price, :int
    add_column :orders, :status, :string
  end
end
