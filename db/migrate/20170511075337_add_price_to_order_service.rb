class AddPriceToOrderService < ActiveRecord::Migration[5.0]
  def change
    add_column :order_services, :price, :int
  end
end
