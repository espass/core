class AddBaseFieldToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :name, :string
    add_column :user_profiles, :address, :string
    add_column :user_profiles, :description, :text
    add_column :user_profiles, :avatar, :text
    add_column :user_profiles, :location, :text
  end
end
