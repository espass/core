class AddRefForUserDevice < ActiveRecord::Migration[5.0]
  def self.up
    add_reference :user_devices, :user, foreign_key: true
    add_reference :user_devices, :device, foreign_key: true
  end

  def self.down
    remove_reference :user_devices, :user, foreign_key: true
    remove_reference :user_devices, :device, foreign_key: true
  end
end
