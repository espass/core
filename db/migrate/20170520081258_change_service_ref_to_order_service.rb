class ChangeServiceRefToOrderService < ActiveRecord::Migration[5.0]
  def change
    remove_reference :order_services, :service, foreign_key: true
    add_reference :order_services, :partner_service, foreign_key: true
  end
end
