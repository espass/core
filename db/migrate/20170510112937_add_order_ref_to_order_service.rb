class AddOrderRefToOrderService < ActiveRecord::Migration[5.0]
  def change
    add_reference :order_services, :order, foreign_key: true
  end
end
