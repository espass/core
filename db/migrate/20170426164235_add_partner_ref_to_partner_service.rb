class AddPartnerRefToPartnerService < ActiveRecord::Migration[5.0]
  def change
    add_reference :partner_services, :partner, foreign_key: true
  end
end
