class AddDeviceTokenToPartnerProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_profiles, :device_token, :string
  end
end
