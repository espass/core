class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.string :target_type
      t.string :target
      t.string :title
      t.string :thumbnail
      t.integer :recipient, index: true
      t.string :recipient_name, index: true
      t.boolean :is_deleted, index: true

      t.timestamps
    end
  end
end
