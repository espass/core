class CreateOrderBookingLines < ActiveRecord::Migration[5.0]
  def change
    create_table :order_booking_lines do |t|
      t.string :status
      t.datetime :finished_at
      t.datetime :started_at
      t.integer :num_partner

      t.datetime :expiry_at
      t.integer :assigned_to
      t.integer :request_id # Current request

      t.timestamps
    end

    add_reference :order_booking_lines, :order, foreign_key: true
  end
end
