class AddBaseFieldToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :mobile, :string
    add_column :users, :email, :string, unique: true
    add_column :users, :status, :tinyint
    add_index :users, :email, unique: true
  end
end
