class AddBaseFieldToService < ActiveRecord::Migration[5.0]
  def change
    add_column :services, :name, :string
    add_column :services, :description, :text
    add_column :services, :thumbnail, :text
    add_column :services, :min_price, :int
    add_column :services, :max_price, :int
    add_column :services, :status, :tinyint
  end
end
