class AddFeaturedFlagToPhoto < ActiveRecord::Migration[5.0]
  def self.up
    add_column :partner_photos, :featured, :boolean
    add_index :partner_photos, :featured
  end

  def self.down
    remove_column :partner_photos, :featured, :boolean
  end
end
