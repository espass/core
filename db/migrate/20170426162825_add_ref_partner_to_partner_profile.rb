class AddRefPartnerToPartnerProfile < ActiveRecord::Migration[5.0]
  def change
    add_reference :partner_profiles, :partner, foreign_key: true
  end
end
