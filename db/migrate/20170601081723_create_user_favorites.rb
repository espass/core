class CreateUserFavorites < ActiveRecord::Migration[5.0]
  def change
    create_table :user_favorites do |t|

      t.timestamps
    end

    add_reference :user_favorites, :partner, foreign_key: true
    add_reference :user_favorites, :user, foreign_key: true
  end
end
