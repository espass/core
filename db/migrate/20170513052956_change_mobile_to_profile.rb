class ChangeMobileToProfile < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :mobile, :string
    remove_column :partners, :mobile, :string
    add_column :user_profiles, :mobile, :string
    add_column :partner_profiles, :mobile, :string
  end
end
