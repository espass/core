class AddFinishedAtToOrder < ActiveRecord::Migration[5.0]
  def self.up
    add_column :orders, :finished_at, :datetime
    add_index :orders, :finished_at
  end

  def self.down
    remove_column :orders, :finished_at, :datetime
    remove_index :orders, :finished_at
  end
end
