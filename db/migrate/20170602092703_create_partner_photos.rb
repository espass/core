class CreatePartnerPhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_photos do |t|
      t.string :title
      t.string :link
      t.integer :status
      t.integer :album_id, index: true

      t.timestamps
    end

    add_reference :partner_photos, :partner, foreign_key: true
  end
end
