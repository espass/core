class IndexForBookingTime < ActiveRecord::Migration[5.0]
  def self.up
    add_index :orders, :booking_time
  end

  def self.down
    remove_index :orders, :booking_time
  end
end
