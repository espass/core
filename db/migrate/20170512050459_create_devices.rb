class CreateDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :devices do |t|
      t.string :os
      t.string :os_version
      t.string :lang
      t.string :device_id
      t.string :device_token

      t.timestamps
    end
  end
end
