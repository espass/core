class RemoveDeviceTokenFromUserAndPartner < ActiveRecord::Migration[5.0]
  def change
    remove_column :user_profiles, :device_token, :string
    remove_column :partner_profiles, :device_token, :string
  end
end
