class AddYearExpToPartnerProfile < ActiveRecord::Migration[5.0]
  def self.up
    add_column :partner_profiles, :year_exp, :integer
  end

  def self.down
    remove_column :partner_profiles, :year_exp, :integer
  end
end
