class AddUnreadNotificationToUserProfile < ActiveRecord::Migration[5.0]
  def self.up
    add_column :user_profiles, :num_unread_notification, :integer
    add_column :user_profiles, :unread_notifications, :text
  end

  def self.down
    remove_column :user_profiles, :num_unread_notification, :integer
    remove_column :user_profiles, :unread_notifications, :text
  end
end
