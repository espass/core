class CreateOrderRequestHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :order_request_histories do |t|
      t.string :status
      t.string :note

      t.timestamps
    end

    add_reference :order_request_histories, :partner, foreign_key: true
    add_reference :order_request_histories, :order, foreign_key: true
  end
end
