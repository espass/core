class AddEnabledToWorkingTime < ActiveRecord::Migration[5.0]
  def change
    add_column :working_times, :enabled, :boolean
  end
end
