class CreatePartnerReviews < ActiveRecord::Migration[5.0]
  def self.up
    create_table :partner_reviews do |t|
      t.string :title
      t.text :description
      t.float :rating
      t.boolean :is_approved, index: true
      t.boolean :is_deleted, index: true

      t.timestamps
    end

    add_reference :partner_reviews, :partner, foreign_key: true
    add_reference :partner_reviews, :user, foreign_key: true
    add_reference :partner_reviews, :order, foreign_key: true
  end

  def self.down
    drop_table :partner_reviews
  end
end
