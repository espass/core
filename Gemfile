source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.0.2'
# Use Puma as the app server
gem 'puma', '3.8.2'
# Use SCSS for stylesheets
gem 'sass-rails', '5.0.6'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '3.2.0'
# Use jquery as the JavaScript library
gem 'jquery-rails', '4.3.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '2.5'

group :test do
  gem 'simplecov', '0.14.1', require: false
end

group :development, :test, :staging do
  # Convention
  gem 'rubocop', '0.49.1'
  # A static analysis security 
  gem 'brakeman', require: false
  # Patch-level verification for Bundler
  gem 'bundler-audit'

  gem 'capistrano', '3.6.1'
  gem 'capistrano-bundler', '1.2.0'
  gem 'capistrano-rails', '1.2.3'
  gem 'capistrano-rbenv', '2.1.0'
  gem 'capistrano3-puma', '2.0.0'

  # Debugger
  gem 'pry-rails', '0.3.6'

  # Rename before coding
  gem 'rename', '1.0.2' # rails g rename:app_to your_name

  # Testing using rspec
  gem 'rspec-rails', '3.6.0'

  # Check query N+1
  gem 'bullet', '5.4.2'

  gem 'traceroute', '0.5.0'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen', '3.0.5'
  gem 'web-console', '3.3.0'
end

# Database
gem 'mysql2', '0.4.4'

# Bring convention over configuration to JSON generation.
gem 'active_model_serializers', '0.10.5'

# Easy to manage your environment variables
gem 'figaro', '1.1.1'

# Facebook library
gem 'koala', '2.4.0'

# Use ActiveModel has_secure_password
gem 'bcrypt', '3.1.11'

# sidekiq
gem 'redis-namespace', '1.5.3'
gem 'sidekiq', '5.0.0'
gem 'sinatra', github: 'sinatra', require: nil

# a Ruby API for the Elasticsearch's REST API
gem 'elasticsearch', '5.0.3'

# A super efficient AWS SQS thread based message processor
gem 'shoryuken', '3.0.6'

# Aws sdk
gem 'aws-sdk', '2.9.14'

# Cron jobs
gem 'whenever', '0.9.7'

# paginator
gem 'kaminari', '1.0.1'

# Error tracker, exception logging
gem 'sentry-raven', '2.6.0'
