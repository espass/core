# README

Things Integrated:

* Database: Mysql
* Coding convention: Rubocop
* Environment variable handle
* Deploy using capistrano (rbenv + puma)

API:
* HATEOAS standard with HAL

How to migrate db:
```
rake db:create
rake db:migrate
```
