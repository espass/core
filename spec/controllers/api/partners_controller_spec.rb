require 'rails_helper'

RSpec.describe Api::PartnersController, type: :controller do
  before do
    u_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @user = User.new(u_data)
    @user.refresh_token({})
    @user.save!

    partner_data = {
      id: 1,
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @partner = Partner.new(partner_data)
    @partner.refresh_token({})
    @partner.save!
    partner_profile_data = {
      name: 'Zofy Nguyen',
      address: 'Ha Noi, Viet Nam',
      location: ENV['LOCATION_DEFAULT'],
      partner_id: @partner.id
    }
    Partner::Profile.create!(partner_profile_data)

    # Setup services for partner
    service_data = {
      name: 'Service name'
    }
    @service = BaseService.new(service_data)
    @service.save!

    p_service_data = {
      name: 'Partner service name',
      partner_id: @partner.id,
      service_id: @service.id
    }
    @partner_service = Partner::Service.new(p_service_data)
    @partner_service.save!

    # Setup working times for partner
    working_time_data = {
      from: 28800,
      to: 61200,
      repeat: %w[0 1 2 3 4 5 6],
      partner_id: @partner.id
    }
    @working_time = Partner::WorkingTime.new(working_time_data)
    @working_time.save!

    es = ElasticsearchServices::PartnerIndex.new
    es.insert_document(@partner)

    p 'Sleep 5 seconds to searchable'
    sleep 5
  end

  after do
    Partner::WorkingTime.destroy_all
    Partner::Service.destroy_all
    Partner::Profile.destroy_all
    BaseService.destroy_all
    Partner.destroy_all
    User.destroy_all
  end

  describe 'GET #index' do
    it 'should failed if the request is not authorized' do
      get :index, params: {
        location: '21.003357, 105.836095',
        order_time: '2017-05-21 15:00:00 +0700',
        services: [{ service_id: @partner_service.service_id }]
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success and result contains the partner' do
      get :index, params: {
        auth_token: @user.auth_token,
        location: '21.003357, 105.836095',
        order_time: '2017-05-21 15:00:00 +0700',
        services: [{ service_id: @partner_service.service_id }]
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['partners'].count).to eq(1)
    end
  end

  describe 'GET #search' do
    it 'should failed if the request is not authorized' do
      get :search, params: {
        q: @partner_service.name
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success and result contains the partner indexed' do
      get :search, params: {
        auth_token: @user.auth_token,
        q: @partner_service.name
      }
      result = JSON.parse(response.body)
      right_ = result['partners'].detect do |partner|
        partner['id'] = @partner.id
      end

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['partners']).not_to eq(0)
      expect(right_).not_to eq(nil)
    end
  end

  describe 'GET #nearby' do
    it 'should failed if the request is not authorized' do
      get :nearby, params: {
        location: ENV['LOCATION_DEFAULT']
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if the request have no valid params' do
      get :nearby, params: {
        auth_token: @user.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(I18n.t('errors.messages.invalid_params'))
    end

    it 'should return no partner if have no partner valid to location' do
      england_location = '52.7611996,-6.8076919'
      get :nearby, params: {
        auth_token: @user.auth_token,
        location: england_location
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['partners']).to eq([])
    end

    it 'should return the partner that it is valid to location' do
      get :nearby, params: {
        auth_token: @user.auth_token,
        location: ENV['LOCATION_DEFAULT']
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['partners'].count).to eq(1)
    end
  end

  describe 'GET #profile' do
    it 'should failed if the request is not authorized' do
      get :profile, params: {
        id: @partner.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should return 404 if the partner is not exist' do
      get :profile, params: {
        id: 6999,
        auth_token: @user.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(404)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(404)
      expect(result['error']['message']).to match(I18n.t('errors.messages.not_found'))
    end

    it 'should success and return standard response' do
      get :profile, params: {
        auth_token: @user.auth_token,
        id: @partner.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['id']).to eq(@partner.id)
      expect(result['profile']['location']).to eq(@partner.profile.location)
      expect(result['services'].count).to eq(1)
    end
  end

  describe 'POST #sign_in' do
    it 'should failed with http 422 status code if it comes with invalid params' do
      post :sign_in
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(I18n.t('errors.messages.invalid_params'))
    end

    it 'should failed with wrong account information' do
      post :sign_in, params: {
        email: @partner.email,
        password: 'wrong_password'
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(I18n.t('errors.messages.invalid_email_or_password'))
    end

    it 'should has a new auth_token after the partner logged in' do
      post :sign_in, params: {
        email: @partner.email,
        password: @partner.password
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@partner.auth_token).not_to eq(result['auth_token'])
    end

    it 'should successfully with right response' do
      post :sign_in, params: {
        email: @partner.email,
        password: @partner.password
      }
      result = JSON.parse(response.body)
      @partner.reload

      expect(result['email']).to eq(@partner.email)
      expect(result['auth_token']).to eq(@partner.auth_token)
      expect(result['links']['profile']).to eq(profile_partner_path(@partner))
    end
  end

  describe 'POST #sign_out' do
    it 'should failed with http 401 status code if request was unauthorized' do
      post :sign_out
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should generate a new auth token for the partner and standard response' do
      params = { auth_token: @partner.auth_token }
      post :sign_out, params: params
      @partner.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@partner.auth_token).not_to eq(params[:auth_token])
    end
  end
end
