require 'rails_helper'

RSpec.describe Api::IssuesController, type: :controller do
  before do
    u_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @user = User.new(u_data)
    @user.refresh_token({})
    @user.save!

    p_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @partner = Partner.new(p_data)
    @partner.refresh_token({})
    @partner.save!
  end

  after do
    Issue.destroy_all
    User.destroy_all
    Partner.destroy_all
  end

  describe 'POST #create' do
    it 'should failed if the request is not authorized' do
      post :create
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if the request has invalid params' do
      post :create, params: {
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(
        I18n.t('activerecord.errors.models.issue.attributes.description.blank')
      )
    end

    it 'should success with an http 200 status code and standard data' do
      post :create, params: {
        auth_token: @partner.auth_token,
        description: 'description'
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(Issue.count).to eq(1)
    end
  end
end
