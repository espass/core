require 'rails_helper'

RSpec.describe Api::ServicesController, type: :controller do
  before do
    # Setup user
    u_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @user = User.new(u_data)
    @user.refresh_token({})
    @user.save!

    # Setup service data
    s_data = {
      name: 'service name',
      description: 'this is description',
      thumbnail: 'no thumbnail',
      min_price: 29000,
      max_price: 69000,
      status: 'Status'
    }

    @service = BaseService.new(s_data)
    @service.save!
  end

  after do
    BaseService.destroy_all
    User.destroy_all
  end

  describe 'GET #index' do
    it 'should failed with an http 401 status code' do
      get :index

      expect(response.content_type).to eq('application/json')
      expect(response).to have_http_status(401)
    end

    it 'should success with an http 200 status code and standard data' do
      get :index, params: {
        auth_token: @user.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['services'].count).to match(1)
      expect(result['links']['self']).to match(list_services_path)
    end
  end
end
