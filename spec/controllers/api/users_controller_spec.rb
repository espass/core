require 'rails_helper'

RSpec.describe Api::UsersController, type: :controller do
  before do
    # Setup user
    u_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @user = User.new(u_data)
    @user.refresh_token({})
    @user.save!

    profile_data = {
      location: ENV['LOCATION_DEFAULT'],
      mobile: '0123456789',
      address: 'The heaven',
      name: 'The name',
      description: 'Information',
      avatar: 'Link to show my beauty',
      user_id: @user.id
    }
    @profile = User::Profile.new(profile_data)
    @profile.save!

    partner_data = {
      id: 1,
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @partner = Partner.new(partner_data)
    @partner.refresh_token({})
    @partner.save!
    partner_profile_data = {
      name: 'Zofy Nguyen',
      address: 'Ha Noi, Viet Nam',
      location: ENV['LOCATION_DEFAULT'],
      partner_id: @partner.id
    }
    Partner::Profile.create!(partner_profile_data)

    @sign_up_params = {
      email: 'second_user@espass.vn',
      password: 'improve_your_self',
      location: ENV['LOCATION_DEFAULT'],
      mobile: '0123456789',
      address: 'The heaven',
      name: 'The name',
      description: 'Information',
      avatar: 'Link to show my beauty',
      dios: 66
    }
  end

  after do
    User::Favorite.destroy_all
    User::Profile.destroy_all
    User.destroy_all
    Partner::Profile.destroy_all
    Partner.destroy_all
  end

  describe 'GET #profile' do
    it 'should failed with http 401 status code if request was unauthorized' do
      get :profile, params: { id: @user.id }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should return a standard response' do
      get :profile, params: { id: @user.id, auth_token: @user.auth_token }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')

      expect(result['email']).to eq(@user.email)
      expect(result['profile']['name']).to eq(@profile.name)
      expect(result['profile']['mobile']).to eq(@profile.mobile)
      expect(result['profile']['address']).to eq(@profile.address)
      expect(result['profile']['location']).to eq(@profile.location)
      expect(result['profile']['description']).to eq(@profile.description)
      expect(result['profile']['avatar']).to eq(@profile.avatar)
    end
  end

  describe 'POST #sign_up' do
    it 'should failed with http 422 status code if it comes with invalid params' do
      post :sign_up
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(I18n.t('errors.messages.invalid_params'))
    end

    it 'should failed if email is taken' do
      post :sign_up, params: {
        email: 'zofy@espass.vn',
        password: 'improve_your_self'
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(
        I18n.t('activerecord.errors.models.user.attributes.email.taken')
      )
    end

    it 'should update some optional params' do
      post :sign_up, params: @sign_up_params
      latest_user = User.last
      latest_user_profile = latest_user.profile

      expect(response).to have_http_status(200)
      expect(User.count).to eq(2)
      expect(latest_user_profile.location).to eq(@sign_up_params[:location])
      expect(latest_user_profile.mobile).to eq(@sign_up_params[:mobile])
      expect(latest_user_profile.address).to eq(@sign_up_params[:address])
      expect(latest_user_profile.name).to eq(@sign_up_params[:name])
      expect(latest_user_profile.description).to eq(@sign_up_params[:description])
      expect(latest_user_profile.avatar).to eq(@sign_up_params[:avatar])
      expect(latest_user.device_id).to eq(@sign_up_params[:dios])
    end

    it 'should success and has a valid auth token' do
      post :sign_up, params: @sign_up_params
      last_user = User.last
      result = JSON.parse(response.body)

      expect(result['auth_token']).to eq(last_user.auth_token)
      expect(result['auth_token'][0]).to eq('U')
    end
  end

  describe 'POST #sign_in' do
    it 'should failed with http 422 status code if it comes with invalid params' do
      post :sign_in
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(I18n.t('errors.messages.invalid_params'))
    end

    it 'should failed with wrong account information' do
      post :sign_in, params: {
        email: @user.email,
        password: 'wrong_password'
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(I18n.t('errors.messages.invalid_email_or_password'))
    end

    it 'should has a new auth_token after the user logged in' do
      post :sign_in, params: {
        email: @user.email,
        password: @user.password
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@user.auth_token).not_to eq(result['auth_token'])
    end

    it 'should successfully with right response' do
      post :sign_in, params: {
        email: @user.email,
        password: @user.password
      }
      result = JSON.parse(response.body)
      @user.reload

      expect(result['email']).to eq(@user.email)
      expect(result['auth_token']).to eq(@user.auth_token)
      expect(result['links']['profile']).to eq(profile_user_path(@user))
    end
  end

  describe 'POST #auth_facebook' do
    it 'should failed with http 422 status code if it comes with invalid params' do
      post :auth_facebook
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(
        I18n.t('errors.messages.invalid_params')
      )
    end

    it 'should failed when request has wrong access token' do
      post :auth_facebook, params: {
        access_token: 'wrong_access_token'
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(
        I18n.t('errors.messages.invalid_oauth_access_token')
      )
    end
  end

  describe 'POST #sign_out' do
    it 'should failed with http 401 status code if request was unauthorized' do
      post :sign_out
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should generate a new auth token for the user and standard response' do
      params = { auth_token: @user.auth_token }
      post :sign_out, params: params
      @user.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@user.auth_token).not_to eq(params[:auth_token])
    end
  end

  describe 'POST #update_profile' do
    it 'should failed when someone unauthorized trying to access' do
      profile_params = { id: @user.id }
      post :update_profile, params: profile_params
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if user is not exist' do
      profile_params = { id: 66, auth_token: @user.auth_token }
      post :update_profile, params: profile_params
      result = JSON.parse(response.body)

      expect(response).to have_http_status(404)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(404)
      expect(result['error']['message']).to match(I18n.t('errors.messages.not_found'))
    end

    it 'should update profile data' do
      profile_params = {
        auth_token: @user.auth_token,
        id: @user.id,
        name: 'The name',
        mobile: '012345678',
        address: 'The heaven',
        location: ENV['LOCATION_DEFAULT'],
        description: 'Information',
        avatar: 'Show'
      }
      post :update_profile, params: profile_params
      @user.reload
      profile = @user.profile

      expect(profile.mobile).to eq(profile_params[:mobile])
      expect(profile.address).to eq(profile_params[:address])
      expect(profile.location).to eq(profile_params[:location])
      expect(profile.description).to eq(profile_params[:description])
      expect(profile.avatar).to eq(profile_params[:avatar])
    end

    it 'should return a standard response' do
      profile_params = {
        auth_token: @user.auth_token,
        id: @user.id
      }
      post :update_profile, params: profile_params
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['email']).to eq(@user.email)
    end
  end

  describe 'POST #update_mobile' do
    it 'should failed when someone unauthorized trying to access' do
      post :update_mobile
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to eq(401)
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if the mobile is verified' do
      mobile_number_for_test = '0123456789'
      @profile.mobile = mobile_number_for_test
      @profile.is_verified = true
      @profile.save

      post :update_mobile, params: {
        auth_token: @user.auth_token,
        mobile: mobile_number_for_test
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to eq(422)
      expect(result['error']['message']).to eq(I18n.t('resources.user.verified'))
    end

    it 'should success and create a pin code' do
      mobile_number_for_test = '0123456789'

      post :update_mobile, params: {
        auth_token: @user.auth_token,
        mobile: mobile_number_for_test
      }
      @profile.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@profile.pin).not_to eq(nil)
      expect(@profile.pin_sent_at).not_to eq(nil)
    end
  end

  describe 'POST #verify_mobile' do
    it 'should failed when someone unauthorized trying to access' do
      post :verify_mobile
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to eq(401)
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if the pin code is invalid' do
      post :verify_mobile, params: {
        auth_token: @user.auth_token,
        pin_code: 'XYZ'
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to eq(422)
      expect(result['error']['message']).to eq(I18n.t('resources.user.invalid_pin'))
    end

    it 'should failed if time expired' do
      @profile.pin = 'XYZ'
      @profile.pin_sent_at = Time.now - 1.years
      @profile.save

      post :verify_mobile, params: {
        auth_token: @user.auth_token,
        pin_code: @profile.pin
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to eq(422)
      expect(result['error']['message']).to eq(I18n.t('resources.user.invalid_pin'))
    end

    it 'should success if the pin code is valid' do
      @profile.pin = 'XYZ'
      @profile.pin_sent_at = Time.now
      @profile.save

      post :verify_mobile, params: {
        auth_token: @user.auth_token,
        pin_code: @profile.pin
      }
      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
    end
  end

  describe 'POST #favorite_paprtner' do
    it 'should failed when someone unauthorized trying to access' do
      post :favorite_partner
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to eq(401)
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed when the favorite data could not be saved' do
      post :favorite_partner, params: {
        auth_token: @user.auth_token,
        partner_id: 69
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to eq(422)
      expect(result['error']['message']).to eq('partner must exist')
      expect(result['links']['self']).to eq(favorite_partner_users_path)
    end

    it 'should success if everything is ok' do
      post :favorite_partner, params: {
        auth_token: @user.auth_token,
        partner_id: @partner.id
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
    end
  end

  describe 'GET #my_partners' do
    it 'should failed when someone unauthorized trying to access' do
      post :my_partners
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to eq(401)
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success if everything is ok' do
      post :my_partners, params: {
        auth_token: @user.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result.key?('partners')).to eq(true)
      expect(result['links']['self']).to eq(my_partners_users_path)
    end
  end
end
