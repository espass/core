require 'rails_helper'

RSpec.describe Api::SettingsController, type: :controller do
  before do
    u_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @user = User.new(u_data)
    @user.refresh_token({})
    @user.save!
  end

  after do
    User.destroy_all
  end

  describe 'GET #index' do
    it 'should success with an http 200 status code and standard data' do
      get :index

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(response.body).to eq(Partner::SettingSerializer.new('').to_json)
    end
  end

  describe 'GET #price_list' do
    it 'should failed if the request is not authorized' do
      get :price_list
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success with an http 200 status code and standard data' do
      get :price_list, params: {
        auth_token: @user.auth_token
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
    end
  end
end
