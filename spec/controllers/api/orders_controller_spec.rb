require 'rails_helper'

RSpec.describe Api::OrdersController, type: :controller do
  before do
    u_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @user = User.new(u_data)
    @user.refresh_token({})
    @user.save!

    p_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @partner = Partner.new(p_data)
    @partner.refresh_token({})
    @partner.save!

    @partner2 = Partner.create(email: 'metal@espass.vn', password: '12345678')

    service_data = {
      name: 'Service name'
    }
    @service = BaseService.new(service_data)
    @service.save!

    p_service_data = {
      name: 'Partner service name',
      partner_id: @partner.id,
      service_id: @service.id
    }
    @partner_service = Partner::Service.new(p_service_data)
    @partner_service.save!

    @order_params = {
      'user_id' => @user.id,
      'partner_id' => @partner.id,
      'price' => 69000,
      'booking_time' => '2017-05-11 11:14:49 +0700',
      'location' => ENV['LOCATION_DEFAULT'],
      'address' => 'Ha Noi, Viet Nam',
      'services' => [
        { 'price' => 69000, 'partner_service_id' => @partner_service.id }
      ],
      'base_services' => [@service.id],
      'attachments' => %w[link]
    }
  end

  after do
    Order::Service.destroy_all
    Partner::Service.destroy_all
    Order.destroy_all
    BaseService.destroy_all
    Partner.destroy_all
    User.destroy_all
  end

  describe 'GET #index' do
    before do
      order_service = OrderServices.new(@order_params)
      @order = order_service.create(@user)
    end

    it 'should failed if the request is not authorized' do
      get :index
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success and return standard response' do
      get :index, params: {
        auth_token: @user.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['orders'].count).to eq(@user.orders.count)

      order = result['orders'][0]
      expect(order['id']).to eq(@order.id)
      expect(order['location']).to eq(@order.location)
      expect(order['address']).to eq(@order.address)
      expect(order['price']).to eq(@order.price)
      expect(order['status']).to eq(@order.status)
      expect(order['base_services'].count).to eq(1)
    end

    it 'should success and return standard response' do
      get :index, params: {
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['orders'].count).to eq(1)

      order = result['orders'][0]
      expect(order['id']).to eq(@order.id)
      expect(order['location']).to eq(@order.location)
      expect(order['address']).to eq(@order.address)
      expect(order['price']).to eq(@order.price)
      expect(order['status']).to eq(@order.status)

      expect(order['base_services'].count).to eq(1)
    end
  end

  describe 'GET #coming-soon' do
    it 'should failed if the request is not authorized' do
      get :coming_soon
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success and return right response' do
      Order.create(
        partner_id: @partner.id,
        user_id: @user.id,
        booking_time: Time.now + 5.days,
        location: ENV['LOCATION_DEFAULT'],
        address: 'Ha Noi, Viet Nam'
      )
      get :coming_soon, params: {
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response.content_type).to eq('application/json')
      expect(result['orders']).not_to eq(nil)
      expect(result['orders'].count).to eq(1)
    end

    it 'should success and return right response' do
      get :coming_soon, params: {
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response.content_type).to eq('application/json')
      expect(result['orders']).not_to eq(nil)
      expect(result['orders'].count).to eq(0)
    end
  end

  describe 'GET #history' do
    it 'should failed if the request is not authorized' do
      get :history
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success and return right response' do
      Order.create(
        partner_id: @partner.id,
        user_id: @user.id,
        booking_time: Time.now + 5.days,
        location: ENV['LOCATION_DEFAULT'],
        address: 'Ha Noi, Viet Nam',
        status: Constants::OrderStatus::SUCCESS
      )
      get :history, params: {
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response.content_type).to eq('application/json')
      expect(result['orders']).not_to eq(nil)
      expect(result['orders'].count).to eq(1)
    end

    it 'should success and return right response' do
      get :history, params: {
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response.content_type).to eq('application/json')
      expect(result['orders']).not_to eq(nil)
      expect(result['orders'].count).to eq(0)
    end
  end

  describe 'GET #scheduled' do
    it 'should failed if the request is not authorized' do
      get :scheduled
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success and return scheduled orders' do
      Order.create(
        partner_id: @partner.id,
        user_id: @user.id,
        booking_time: Time.now + 5.days,
        location: ENV['LOCATION_DEFAULT'],
        address: 'Ha Noi, Viet Nam',
        status: Constants::OrderStatus::PENDING
      )
      get :scheduled, params: {
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response.content_type).to eq('application/json')
      expect(result['orders']).not_to eq(nil)
      expect(result['orders'].count).to eq(1)
    end

    it 'should success and dont return success order' do
      Order.create(
        partner_id: @partner.id,
        user_id: @user.id,
        booking_time: Time.now + 5.days,
        location: ENV['LOCATION_DEFAULT'],
        address: 'Ha Noi, Viet Nam',
        status: Constants::OrderStatus::SUCCESS
      )
      get :scheduled, params: {
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response.content_type).to eq('application/json')
      expect(result['orders']).not_to eq(nil)
      expect(result['orders'].count).to eq(0)
    end

    it 'should success and return right response' do
      get :scheduled, params: {
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response.content_type).to eq('application/json')
      expect(result['orders']).not_to eq(nil)
      expect(result['orders'].count).to eq(0)
    end
  end

  describe 'POST #create' do
    it 'should failed if the request is not authorized' do
      post :create, params: @order_params
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if the request is missing params' do
      post :create, params: {
        auth_token: @user.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(I18n.t('errors.messages.invalid_params'))
    end

    it 'should failed if services params is wrong format' do
      @order_params['services'] = @order_params['services'][0]
      post :create, params: @order_params.merge(auth_token: @user.auth_token)
      result = JSON.parse(response.body)

      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(
        I18n.t('internal.errors.messages.wrong_format_hash', field: 'services')
      )
    end

    it 'should failed if something related to order failed' do
      @order_params['services'] = [{ 'price' => 69000 }]
      post :create, params: @order_params.merge(auth_token: @user.auth_token)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to eq('partner_service must exist')
    end

    it 'should success with a standard response' do
      post :create, params: @order_params.merge(auth_token: @user.auth_token)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['location']).to eq(@order_params['location'])
      expect(result['address']).to eq(@order_params['address'])
      expect(result['price']).to eq(@order_params['price'])
      expect(result['status']).to eq(Constants::OrderStatus::PENDING)
      expect(result['links']['self']).to match(create_orders_path)
    end

    it 'should hit something into data' do
      post :create, params: @order_params.merge(auth_token: @user.auth_token)

      expect(Order.count).to eq(1)
      expect(Order::Service.count).to eq(1)
    end
  end

  describe 'POST #create_quickly' do
    it 'should failed if the request is not authorized' do
      post :create_quickly, params: @order_params
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if request has invalid params' do
      post :create_quickly
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.invalid_params'))
    end

    it 'should failed if request has wrong format param' do
      @order_params['attachments'] = 'wrong format'
      post :create_quickly, params: @order_params.merge(auth_token: @user.auth_token)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(
        I18n.t('internal.errors.messages.wrong_format', field: 'attachments')
      )
    end

    it 'should success if everything is ok' do
      num_order = Order.count
      post :create_quickly, params: @order_params.merge(auth_token: @user.auth_token)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(Order.count).to eq(num_order + 1)
    end
  end

  describe 'POST #accept' do
    before do
      @order = Order.create(
        user_id: @user.id,
        booking_time: Time.now + 1.days,
        location: ENV['LOCATION_DEFAULT'],
        status: Constants::OrderStatus::REQUESTED,
        address: 'Ha Noi, Viet Nam'
      )
    end

    after do
      Order::RequestHistory.destroy_all
      Order::BookingLine.destroy_all
    end

    it 'should failed if the request is not authorized' do
      post :accept, params: {
        id: @order.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if status of order is not requested' do
      @order.update(status: Constants::OrderStatus::SUCCESS)
      request = Order::RequestHistory.create(
        partner_id: @partner.id,
        order_id: @order.id
      )
      Order::BookingLine.create(
        assigned_to: @partner.id,
        order_id: @order.id,
        request_id: request.id
      )

      post :accept, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_accept'))
    end

    it 'should failed if order has no booking line' do
      post :accept, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_accept'))
    end

    it 'should failed if request of booking line has not yet created' do
      Order::BookingLine.create(
        assigned_to: @partner.id,
        order_id: @order.id
      )
      post :accept, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_accept'))
    end

    it 'should failed if the partner has no permission' do
      request = Order::RequestHistory.create(
        partner_id: @partner.id,
        order_id: @order.id
      )
      Order::BookingLine.create(
        order_id: @order.id,
        request_id: request.id
      )
      post :accept, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_accept'))
    end

    it 'should failed if the partner is busy in booking time' do
      @order.update(partner_id: @partner.id)
      request = Order::RequestHistory.create(
        partner_id: @partner.id,
        order_id: @order.id
      )
      Order::BookingLine.create(
        order_id: @order.id,
        assigned_to: @partner.id,
        request_id: request.id
      )
      post :accept, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(partner_id: nil)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_accept'))
    end

    it 'should failed if the order could not be saved' do
      Order.update_all(location: nil)

      request = Order::RequestHistory.create(
        partner_id: @partner.id,
        order_id: @order.id
      )
      Order::BookingLine.create(
        order_id: @order.id,
        assigned_to: @partner.id,
        request_id: request.id
      )
      post :accept, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match('location')
    end

    it 'should success if everything is ok' do
      request = Order::RequestHistory.create(
        partner_id: @partner.id,
        order_id: @order.id
      )
      Order::BookingLine.create(
        order_id: @order.id,
        assigned_to: @partner.id,
        request_id: request.id
      )
      post :accept, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)
      @order.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@order.status).to eq(Constants::OrderStatus::PENDING)
      expect(@order.partner_id).to eq(@partner.id)
      expect(result['id']).to eq(@order.id)
    end
  end

  describe 'POST #decline' do
    before do
      @order = Order.create(
        user_id: @user.id,
        booking_time: Time.now + 1.days,
        location: ENV['LOCATION_DEFAULT'],
        status: Constants::OrderStatus::REQUESTED,
        address: 'Ha Noi, Viet Nam'
      )
    end

    after do
      Order::RequestHistory.destroy_all
      Order::BookingLine.destroy_all
    end

    it 'should failed if the request is not authorized' do
      post :decline, params: {
        id: @order.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if status of order is not requested' do
      @order.update(status: Constants::OrderStatus::SUCCESS)
      request = Order::RequestHistory.create(
        partner_id: @partner.id,
        order_id: @order.id
      )
      Order::BookingLine.create(
        assigned_to: @partner.id,
        order_id: @order.id,
        request_id: request.id
      )

      post :decline, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_decline'))
    end

    it 'should failed if request of booking line has not yet created' do
      Order::BookingLine.create(
        assigned_to: @partner.id,
        order_id: @order.id
      )
      post :decline, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_decline'))
    end

    it 'should failed if the partner has no permission' do
      request = Order::RequestHistory.create(
        partner_id: @partner.id,
        order_id: @order.id
      )
      Order::BookingLine.create(
        order_id: @order.id,
        request_id: request.id
      )
      post :decline, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_decline'))
    end

    it 'should success if everything is ok' do
      req = Order::RequestHistory.create(
        partner_id: @partner.id,
        order_id: @order.id
      )
      Order::BookingLine.create(
        order_id: @order.id,
        assigned_to: @partner.id,
        request_id: req.id
      )
      post :decline, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      req.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(req.status).to eq(Constants::BookingRequestStatus::DECLINED)
    end
  end

  describe 'POST #partner_process' do
    before do
      @order = Order.create(
        user_id: @user.id,
        booking_time: Time.now + 1.days,
        location: ENV['LOCATION_DEFAULT'],
        status: Constants::OrderStatus::REQUESTED,
        address: 'Ha Noi, Viet Nam'
      )
    end

    after do
      Order::RequestHistory.destroy_all
      Order::BookingLine.destroy_all
    end

    it 'should failed if the request is not authorized' do
      post :partner_process, params: {
        id: @order.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if status of order is not pending' do
      @order.update(status: Constants::OrderStatus::SUCCESS)
      post :partner_process, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_process'))
    end

    it 'should failed if the partner has no permission' do
      @order.update(status: Constants::OrderStatus::PENDING)
      post :partner_process, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_process'))
    end

    it 'should failed if the order could not be saved' do
      Order.update_all(location: nil)
      @order.update(status: Constants::OrderStatus::PENDING, partner_id: @partner.id)
      post :partner_process, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match('location')
    end

    it 'should success if everything is ok' do
      @order.update(status: Constants::OrderStatus::PENDING, partner_id: @partner.id)
      post :partner_process, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@order.status).to eq(Constants::OrderStatus::PROCESSING)
    end
  end

  describe 'POST #finish' do
    before do
      @order = Order.create(
        user_id: @user.id,
        booking_time: Time.now + 1.days,
        location: ENV['LOCATION_DEFAULT'],
        status: Constants::OrderStatus::REQUESTED,
        address: 'Ha Noi, Viet Nam'
      )
    end

    after do
      Order::RequestHistory.destroy_all
      Order::BookingLine.destroy_all
    end

    it 'should failed if the request is not authorized' do
      post :finish, params: {
        id: @order.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if status of order is not processing' do
      @order.update(status: Constants::OrderStatus::SUCCESS)
      post :finish, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_finish'))
    end

    it 'should failed if the partner has no permission' do
      @order.update(status: Constants::OrderStatus::PROCESSING)
      post :finish, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_finish'))
    end

    it 'should failed if the order could not be saved' do
      Order.update_all(location: nil)
      @order.update(status: Constants::OrderStatus::PROCESSING, partner_id: @partner.id)
      post :finish, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match('location')
    end

    it 'should success if everything is ok' do
      @order.update(status: Constants::OrderStatus::PROCESSING, partner_id: @partner.id)
      post :finish, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@order.status).to eq(Constants::OrderStatus::SUCCESS)
    end
  end

  describe 'POST #cancel' do
    before do
      @order = Order.create(
        user_id: @user.id,
        booking_time: Time.now + 1.days,
        location: ENV['LOCATION_DEFAULT'],
        status: Constants::OrderStatus::REQUESTED,
        address: 'Ha Noi, Viet Nam'
      )
    end

    after do
      Order::RequestHistory.destroy_all
      Order::BookingLine.destroy_all
    end

    it 'should failed if the request is not authorized' do
      post :cancel, params: {
        id: @order.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if status of order is not valid' do
      @order.update(status: Constants::OrderStatus::SUCCESS)
      post :cancel, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_cancel'))
    end

    it 'should failed if the partner has no permission' do
      @order.update(status: Constants::OrderStatus::PENDING)
      post :cancel, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('resources.order.could_not_cancel'))
    end

    it 'should failed if the order could not be saved' do
      Order.update_all(location: nil)
      @order.update(status: Constants::OrderStatus::PENDING, partner_id: @partner.id)
      post :cancel, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.update(status: Constants::OrderStatus::REQUESTED)
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match('location')
    end

    it 'should failed if the order is not exist' do
      post :cancel, params: {
        id: 66666,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(404)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.not_found'))
    end

    it 'should success if everything is ok' do
      @order.update(status: Constants::OrderStatus::PENDING, partner_id: @partner.id)
      post :cancel, params: {
        id: @order.id,
        auth_token: @partner.auth_token
      }
      @order.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@order.status).to eq(Constants::OrderStatus::CANCELED)
    end
  end
end
