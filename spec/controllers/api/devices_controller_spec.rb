require 'rails_helper'

RSpec.describe Api::DevicesController, type: :controller do
  after do
    GlobalDevice.destroy_all
  end

  describe 'POST #register' do
    it 'should failed with an http 422 status code' do
      post :register

      expect(response.content_type).to eq('application/json')
      expect(response).to have_http_status(422)
    end

    it 'should success with an http 200 status code and standard data' do
      post :register, params: {
        os: 'IOS',
        os_version: '10.2',
        lang: 'en',
        device_id: 'espass.edela'
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(response.body).to include('dios')
    end
  end
end
