require 'rails_helper'

RSpec.describe Api::Partners::ReviewsController, type: :controller do
  before do
    u_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @user = User.new(u_data)
    @user.refresh_token({})
    @user.save!

    partner_data = {
      id: 1,
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @partner = Partner.new(partner_data)
    @partner.refresh_token({})
    @partner.save!
    partner_profile_data = {
      name: 'Zofy Nguyen',
      address: 'Ha Noi, Viet Nam',
      location: ENV['LOCATION_DEFAULT'],
      partner_id: @partner.id
    }
    Partner::Profile.create!(partner_profile_data)

    @order = Order.create(
      user_id: @user.id,
      partner_id: @partner.id,
      status: Constants::OrderStatus::SUCCESS,
      booking_time: Time.now + 1.days,
      location: ENV['LOCATION_DEFAULT'],
      address: 'Ha Noi, Viet Nam'
    )

    @review = Partner::Review.create(
      title: 'Title',
      description: 'Description',
      rating: 5.0,
      partner_id: @partner.id,
      user_id: @user.id
    )
  end

  after do
    Partner::RatingReport.destroy_all
    Partner::Review.destroy_all
    Order.destroy_all
    Partner::Profile.destroy_all
    Partner.destroy_all
  end

  describe 'GET #index' do
    it 'should fail if the request is not authorized' do
      get :index, params: {
        id: @partner.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success and return list reviews of partner' do
      get :index, params: {
        id: @partner.id,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result.key?('reviews')).to eq(true)
      expect(result['reviews'].count).to eq(1)
      expect(result['reviews'][0]['title']).to eq(@review.title)
    end
  end

  describe 'POST #create' do
    it 'should fail if the request is not authorized' do
      get :create, params: {
        id: @partner.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if the user has no permission' do
      @order.update(partner_id: nil)
      post :create, params: {
        id: @partner.id,
        auth_token: @user.auth_token,
        order_id: @order.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('resources.partner.review.could_not_review'))
    end

    it 'should fail if the request missing field' do
      post :create, params: {
        id: @partner.id,
        auth_token: @user.auth_token,
        order_id: @order.id
      }

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
    end

    it 'should success if everything is ok' do
      @partner.reviews.destroy_all

      post :create, params: {
        id: @partner.id,
        auth_token: @user.auth_token,
        title: 'Title',
        description: 'Description',
        rating: 5.0,
        order_id: @order.id
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@partner.reviews.count).to eq(1)
    end
  end
end
