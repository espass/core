require 'rails_helper'

RSpec.describe Api::Partners::WorkingTimesController, type: :controller do
  before do
    partner_data = {
      id: 1,
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @partner = Partner.new(partner_data)
    @partner.refresh_token({})
    @partner.save!
    partner_profile_data = {
      name: 'Zofy Nguyen',
      address: 'Ha Noi, Viet Nam',
      location: ENV['LOCATION_DEFAULT'],
      partner_id: @partner.id
    }
    Partner::Profile.create!(partner_profile_data)

    @working_times = Partner::WorkingTime.create(
      [
        { from: 6990, to: 6990, repeat: [1, 2, 3], partner_id: @partner.id },
        { from: 2544, to: 9000, repeat: [0, 1, 3], partner_id: @partner.id }
      ]
    )
  end

  after do
    Partner::WorkingTime.destroy_all
    Partner::Profile.destroy_all
    Partner.destroy_all
  end

  describe 'GET #index' do
    it 'should fail if the request is not authorized' do
      get :index, params: {
        id: @partner.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success with right data' do
      get :index, params: {
        id: @partner.id,
        auth_token: @partner.auth_token
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(response.body).to eq(
        Partner::ListWorkingTimeSerializer.new(
          working_times: @partner.working_times,
          partner: @partner
        ).to_json
      )
    end
  end

  describe 'POST #create' do
    it 'should fail if the request is not authorized' do
      post :create, params: {
        id: @partner.id,
        from: 699000,
        to: 899000,
        repeat: [1, 2, 3]
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if the request has invalid params' do
      post :create, params: {
        id: @partner.id,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.invalid_params'))
    end

    it 'should success with right data' do
      Partner::WorkingTime.destroy_all

      post :create, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        from: 699000,
        to: 899000,
        repeat: [1, 2, 3]
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(Partner::WorkingTime.count).to eq(1)
    end
  end

  describe 'POST #update' do
    it 'should fail if the request is not authorized' do
      post :update, params: {
        id: @partner.id,
        wt_id: @working_times[0].id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if the album can not be saved' do
      Partner::WorkingTime.update_all(from: nil)

      post :update, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        wt_id: @working_times[0].id,
        to: 666666
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match('from')
    end

    it 'should success when everything is ok' do
      wt = @working_times[0]
      post :update, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        wt_id: wt.id,
        from: '666666',
        to: '666666',
        repeat: %w[5 6],
        enabled: false
      }
      wt.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(wt.from).to eq('666666')
      expect(wt.to).to eq('666666')
      expect(wt.repeat).to eq(%w[5 6])
      expect(wt.enabled).to eq(false)
    end
  end

  describe 'POST #delete' do
    it 'should fail if the request is not authorized' do
      post :delete, params: {
        id: @partner.id,
        wt_id: @working_times[0].id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if working time is not exist' do
      post :delete, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        wt_id: 6666
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(404)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.not_found'))
    end

    it 'should success if the working time was deleted' do
      @wt = @working_times[0]
      post :delete, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        wt_id: @wt.id
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(Partner::WorkingTime.where(id: @wt.id).count).to eq(0)
    end
  end
end
