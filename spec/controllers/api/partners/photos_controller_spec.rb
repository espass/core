require 'rails_helper'

RSpec.describe Api::Partners::PhotosController, type: :controller do
  before do
    partner_data = {
      id: 1,
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @partner = Partner.new(partner_data)
    @partner.refresh_token({})
    @partner.save!

    @photo = Partner::Photo.create(
      link: 'http://example.com/link.png',
      title: 'Title',
      partner_id: @partner.id
    )
  end

  after do
    Partner::Photo.destroy_all
  end
end
