require 'rails_helper'

RSpec.describe Api::Partners::AlbumsController, type: :controller do
  before do
    partner_data = {
      id: 1,
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @partner = Partner.new(partner_data)
    @partner.refresh_token({})
    @partner.save!
    partner_profile_data = {
      name: 'Zofy Nguyen',
      address: 'Ha Noi, Viet Nam',
      location: ENV['LOCATION_DEFAULT'],
      partner_id: @partner.id
    }
    Partner::Profile.create!(partner_profile_data)

    @album = Partner::Album.create(
      title: 'I am handsome',
      thumbnail: 'My face',
      partner_id: @partner.id
    )

    @partner2 = Partner.create(email: 'partner2@espass.vn', password: 'passwords')
    @album2 = Partner::Album.create(
      title: 'I am a man',
      thumbnail: 'Thumbnail',
      partner_id: @partner2.id
    )

    photo_data = [
      {
        title: 'This is title of the photo',
        link: 'link',
        partner_id: @partner.id,
        album_id: @album.id
      },
      {
        title: 'This is title of the photo',
        link: 'link',
        partner_id: @partner.id,
        album_id: @album.id
      }
    ]
    Partner::Photo.create(photo_data)
  end

  after do
    Partner::Photo.destroy_all
    Partner::Profile.destroy_all
    Partner::Album.destroy_all
    Partner.destroy_all
  end

  describe 'GET #index' do
    it 'should fail if the request is not authorized' do
      get :index, params: {
        id: @partner.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if partner is not exist' do
      get :index, params: {
        id: 'not_exist',
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(404)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.not_found'))
    end

    it 'should success and return list albums of partner' do
      get :index, params: {
        id: @partner.id,
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result.key?('albums')).to eq(true)
      expect(result['albums'].count).to eq(1)
      expect(result['albums'][0]['title']).to eq(@album.title)
    end
  end

  describe 'POST #create' do
    it 'should fail if the request is not authorized' do
      post :create, params: {
        id: @partner.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if the request missing field' do
      post :create, params: {
        id: @partner.id,
        auth_token: @partner.auth_token
      }

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
    end

    it 'should success if everything is valid' do
      post :create, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        title: 'This is title'
      }
      album = Partner::Album.last

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(response.body).to eq(Partner::AlbumSerializer.new(album).to_json)
    end
  end

  describe 'POST #delete' do
    it 'should fail if the request is not authorized' do
      post :delete, params: {
        id: @partner.id,
        ab_id: @album2.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if current partner has no permission' do
      post :delete, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: @album2.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.no_permission'))
    end

    it 'should fail if the album is not exist' do
      post :delete, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: 'album_id_not_exist'
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(404)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.not_found'))
    end

    it 'should fail if the album can not be saved' do
      Partner::Album.update_all(title: '')
      post :delete, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: @album2.id
      }

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
    end

    it 'should success if the album is mark as deleted' do
      post :delete, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: @album.id
      }
      @album.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@album.is_deleted).to eq(true)
    end
  end

  describe 'GET #photos' do
    it 'should fail if the request is not authorized' do
      post :delete, params: {
        id: @partner.id,
        ab_id: @album2.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should success and return right response' do
      post :photos, params: {
        id: @partner.id,
        ab_id: @album.id,
        auth_token: @partner.auth_token
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(response.body).to eq(ListPhotoSerializer.new(@album).to_json)
    end
  end

  describe 'POST #add_photos' do
    it 'should fail if the request is not authorized' do
      post :add_photos, params: {
        id: @partner.id,
        ab_id: @album.id,
        photos: [{ title: 'Title', link: 'Link' }]
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if the album is not exist' do
      post :add_photos, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: 'album_id_not_exist',
        photos: [{ title: 'Title', link: 'Link' }]
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(404)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.not_found'))
    end

    it 'should fail if the request missing params' do
      post :add_photos, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: @album.id,
        photos: ''
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.invalid_params'))
    end

    it 'should fail if current partner has no permission' do
      post :add_photos, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: @album2.id,
        photos: [{ title: 'Title', link: 'Link' }]
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.no_permission'))
    end

    it 'should succes if everything is ok' do
      @album.photos.destroy_all

      post :add_photos, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: @album.id,
        photos: [{ title: 'Title', link: 'Link' }]
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@album.photos.count).to eq(1)
    end
  end

  describe 'POST #delete_photos' do
    it 'should fail if the request is not authorized' do
      post :delete_photos, params: {
        id: @partner.id,
        ab_id: @album.id,
        photo_ids: @album.photo_ids
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if the album is not exist' do
      post :delete_photos, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: 'album_id_not_exist',
        photo_ids: @album.photo_ids
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(404)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.not_found'))
    end

    it 'should fail if the request missing params' do
      post :delete_photos, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: @album.id,
        photos: ''
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.invalid_params'))
    end

    it 'should fail if current partner has no permission' do
      post :delete_photos, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: @album2.id,
        photo_ids: @album.photo_ids
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to eq(I18n.t('errors.messages.no_permission'))
    end

    it 'should succes if everything is ok' do
      post :delete_photos, params: {
        id: @partner.id,
        auth_token: @partner.auth_token,
        ab_id: @album.id,
        photo_ids: @album.photo_ids
      }

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@album.photos.count).to eq(0)
    end
  end
end
