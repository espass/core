require 'rails_helper'

RSpec.describe Api::LibrariesController, type: :controller do
  before do
    u_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @user = User.new(u_data)
    @user.refresh_token({})
    @user.save!

    p_data = {
      email: 'zofy@espass.vn',
      password: 'password'
    }
    @partner = Partner.new(p_data)
    @partner.refresh_token({})
    @partner.save!
  end

  after do
    Library.destroy_all
    User.destroy_all
    Partner.destroy_all
  end

  describe 'POST #create' do
    it 'should failed if the request is not authorized' do
      post :create
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(401)
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should failed if the request has invalid params' do
      post :create, params: {
        auth_token: @partner.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(
        I18n.t('errors.messages.invalid_params')
      )
    end

    it 'should failed if the file is a large file' do
      media = fixture_file_upload('library/large.jpg')
      post :create, params: {
        auth_token: @partner.auth_token,
        media: media
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['code']).to match(422)
      expect(result['error']['message']).to match(
        I18n.t('resources.library.invalid_media_size')
      )
    end

    it 'should success with right data' do
      media = fixture_file_upload('library/cool.jpg')
      post :create, params: {
        auth_token: @partner.auth_token,
        media: media
      }
      library = Library.first

      expect(library).not_to eq(nil)
      expect(library.partner_id).to eq(@partner.id)
      expect(library.url).not_to eq(nil)
    end

    it 'should success and return right response' do
      media = fixture_file_upload('library/cool.jpg')
      post :create, params: {
        auth_token: @partner.auth_token,
        media: media
      }
      result = JSON.parse(response.body)
      library = Library.first

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(result['url']).to eq(library.url)
    end
  end

  describe 'POST #delete' do
    before do
      @library = Library.create(key: 'test', partner_id: @partner.id)
    end

    it 'should failed if the request is not authorized' do
      post :delete, params: {
        id: @library.id
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(401)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('errors.messages.unauthorized'))
    end

    it 'should fail if the library is not exist' do
      post :delete, params: {
        id: 66666,
        auth_token: @user.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(404)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('errors.messages.not_found'))
    end

    it 'should fail if user has no permission' do
      post :delete, params: {
        id: @library.id,
        auth_token: @user.auth_token
      }
      result = JSON.parse(response.body)

      expect(response).to have_http_status(422)
      expect(response.content_type).to eq('application/json')
      expect(result['error']['message']).to match(I18n.t('errors.messages.no_permission'))
    end

    it 'should success if everything is ok' do
      post :delete, params: {
        id: @library.id,
        auth_token: @partner.auth_token
      }
      @library.reload

      expect(response).to have_http_status(200)
      expect(response.content_type).to eq('application/json')
      expect(@library.is_deleted).to eq(true)
    end
  end
end
