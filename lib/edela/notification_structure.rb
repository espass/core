class NotificationStructure
  attr_accessor :title, :content
  attr_accessor :subscription, :event

  class Subscription
    attr_accessor :endpoint, :protocol, :platform

    def initialize(init_data = nil)
      if init_data.is_a?(Hash)
        init_by_data(init_data)
        return
      end

      @protocol = nil
      @endpoint = nil
      @platform = nil
    end

    private

    def init_by_data(init_data)
      @endpoint = init_data['endpoint']
      @protocol = init_data['protocol']
      @platform = init_data['platform']
    end
  end

  class Event
    attr_accessor :trigged_at, :name, :target, :data

    def initialize(init_data = nil)
      @trigged_at = Time.now
      if init_data.is_a?(Hash)
        init_by_data(init_data)
        return
      end

      @name = name
      @target = ''
      @data = {}
    end

    private

    def init_by_data(init_data)
      @name = init_data['name']
      @data = init_data['data']
      @target = init_data['target']
    end
  end

  def initialize(init_data = nil)
    init_data.is_a?(Hash) ? init_by_data(init_data) : init_default
  end

  def to_json
    super
  end

  private

  def init_by_data(init_data)
    @title = init_data['title']
    @content = init_data['content']
    @subscription = Subscription.new(init_data['subscription'])
    @event = Event.new(init_data['event'])
  end

  def init_default
    @title = nil
    @content = nil
    @subscription = Subscription.new
    @event = Event.new
  end
end
