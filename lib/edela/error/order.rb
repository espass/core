module Edela
  module OrderError
    class FailedCRUD < StandardError
      def to_s
        super
      end
    end

    class FailedToCreate < FailedCRUD; end
    class FailedToUpdate < FailedCRUD; end
    class FailedToDelete < FailedCRUD; end
  end
end
