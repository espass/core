namespace :find_new_partner do
  desc 'Looking for partners'
  task go: :environment do
    orders = Order.joins(:booking_line).where(
      'order_booking_lines.status = ? and order_booking_lines.expiry_at < ?',
      Constants::BookingLineStatus::PROCESSING,
      Time.now
    ).includes(:booking_line)

    orders.each do |order|
      candidate = order.find_new_partner
      break if candidate.nil?
      device = candidate.device
      break if device.nil? || device.notification_available? == false

      push_mobile_notification(
        device, order, Constants::EventTarget::PARTNER, I18n.t('resources.order.requested')
      )
    end
  end

  private

  def push_mobile_notification(device, order, target, title)
    # Send message to notification service to verify mobile number
    noti = NotificationStructure.new
    noti.title = title
    noti.subscription.endpoint = device.device_token
    noti.subscription.protocol = Constants::NotificationProtocol::PLATFORM
    noti.subscription.platform = device.os
    noti.event.name = Constants::NotificationEvent::REQUEST_PARTNER
    noti.event.data = { order: Order::NotificationSerializer.new(order) }
    noti.event.target = target

    Shoryuken::Client.queues(ENV['NOTIFICATION_WORKER_QUEUE']).send_message(message_body: noti.to_json)
  end
end
