class ApiController < ActionController::Base
  attr_reader :current_user, :current_partner
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  def set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, Token'
    headers['Access-Control-Max-Age'] = '1728000'
  end

  # Auth both partner and user
  def authenticate_resource
    auth_token = params[:auth_token]

    unless auth_token.blank?
      resource_class = auth_token[0] == 'U' ? User : Partner
      @current_resource =  resource_class.where(auth_token: auth_token).first

      if resource_class == User
        @current_user = @current_resource
      else
        @current_partner = @current_resource
      end
    end
    return if @current_resource.present? && @current_resource.token_expiry > Time.now

    status_code = 401
    serialize_data = build_serialize_data(
      I18n.t('errors.messages.unauthorized'),
      status_code
    )
    render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
  end

  # Auth user
  def authenticate_user
    auth_token = params[:auth_token]
    unless auth_token.blank?
      @current_user = User.where(auth_token: auth_token).first
    end
    return if current_user.present? && current_user.token_expiry > Time.now

    status_code = 401
    serialize_data = build_serialize_data(
      I18n.t('errors.messages.unauthorized'),
      status_code,
      RelatedLinksHelper.auth_user_fail
    )
    render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
  end

  # Auth partner
  def authenticate_partner
    auth_token = params[:auth_token]
    unless auth_token.blank?
      @current_partner = Partner.where(auth_token: auth_token).first
    end

    return true if current_partner.present? && current_partner.token_expiry > Time.now

    status_code = 401
    serialize_data = build_serialize_data(
      I18n.t('errors.messages.unauthorized'),
      status_code,
      RelatedLinksHelper.auth_partner_fail
    )
    render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
  end

  def validate_partner
    @partner =
      if current_partner && current_partner.is_a?(Partner) && current_partner.id == params[:id].to_i
        current_partner
      else
        Partner.where(id: params[:id]).first
      end

    return true unless @partner.nil?

    status_code = 404
    serialize_data = build_serialize_data(
      I18n.t('errors.messages.not_found'),
      status_code
    )
    render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
  end

  def validate_post_data(data, message, serialize_data_extra = { links: {} })
    data_valid = data.values.all?(&:present?)
    return true if data_valid

    serialize_data_extra[:links][:self] = request.path
    serialize_data = { code: 422, message: message }.merge(serialize_data_extra)
    render json: ErrorSerializer.new(serialize_data).to_json, status: :unprocessable_entity
  end

  # This method allow access current user on models
  def set_current_user
    DataCenter.current_user = @current_user
  end

  def set_current_partner
    DataCenter.current_partner = @current_partner
  end

  def set_current_resource
    DataCenter.current_resource = @current_resource
  end

  def extra_authen_params
    {
      last_sign_in_at: Time.now,
      last_sign_in_ip: request.remote_ip,
      device_id: params[:dios]
    }
  end

  def build_serialize_data(message, http_code = 422, related_links = {})
    serialize_data = {}
    serialize_data[:links] = { self: request.path }.merge(related_links)
    serialize_data[:code] = http_code
    serialize_data[:message] = message
    serialize_data
  end

  def current_page
    params[:p].present? ? params[:p].to_i : 1
  end

  def load_pagination_data(resource)
    {
      current_page: current_page, total_page: resource.total_pages,
      next_page: resource.next_page, prev_page: resource.prev_page
    }
  end
end
