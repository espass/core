module Api
  class UsersController < ApiController
    before_action :authenticate_user, only: %w[
      profile update_profile sign_out update_mobile
      verify_mobile favorite_partner my_partners ping
    ]
    before_action :authenticate_reset_password_token, only: %w[reset_password]
    before_action :validate_user, only: %w[profile update_profile]
    before_action :validate_sign_up_params, only: %w[sign_up]
    before_action :validate_sign_in_params, only: %w[sign_in]
    before_action :validate_auth_fb_params, only: %w[auth_facebook]
    before_action :validate_verify_mobile_params, only: %w[verify_mobile]
    before_action :validate_update_mobile_params, only: %w[update_mobile]
    before_action :validate_favorite_partner_params, only: %w[favorite_partner]
    before_action :validate_forgot_password_params, only: %w[forgot_password]
    after_action :set_access_control_headers, only: %w[reset_password]

    def profile
      render json: @user
    end

    # Required params: email, password
    def sign_up
      serialize_data = { links: { self: sign_up_users_path } }
      user = User.new(sign_up_params)
      # Refresh token and set authen information
      user.refresh_token(extra_authen_params)

      if user.save && user.update_profile(params)
        user.log_device(params)

        serialize_data[:resource] = user
        render json: AuthSerializer.new(serialize_data).to_json
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(user),
          status_code,
          RelatedLinksHelper.sign_up_user_fail
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    # Required params: email, password
    def sign_in
      user = User.where(email: params[:email].strip).first
      serialize_data = { links: { self: sign_in_users_path } }

      if user && user.authenticate(params[:password])
        # Refresh token, set authen information and auto save
        user.refresh_token(extra_authen_params, true)
        user.log_device(params)

        serialize_data[:resource] = user
        render json: AuthSerializer.new(serialize_data).to_json
      else
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('errors.messages.invalid_email_or_password'),
          status_code,
          RelatedLinksHelper.sign_in_user_fail
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    # Required params: access_token
    def auth_facebook
      fb = Koala::Facebook::API.new(params[:access_token])
      serialize_data = { links: { self: auth_facebook_users_path } }

      begin
        oauth = fb.get_object('me')
        oauth['uid'] = oauth['id']
        oauth['provider'] = Constants::AuthProvider::FB

        user = User.handle_for_oauth(oauth, extra_authen_params)

        serialize_data[:resource] = user
        render json: AuthSerializer.new(serialize_data).to_json
      rescue Koala::Facebook::AuthenticationError
        # Invalid OAuth access token
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('errors.messages.invalid_oauth_access_token'),
          status_code,
          RelatedLinksHelper.auth_user_fail
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    def ping
      render nothing: true
    end

    # Required params: auth_token
    def sign_out
      current_user.refresh_token({}, true)

      serialize_data = {
        links: { self: sign_in_users_path },
        resource: current_user
      }

      render json: AuthSerializer.new(serialize_data).to_json
    end

    def update_profile
      if current_user.update_profile(params)
        render json: @user
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(@user),
          status_code,
          RelatedLinksHelper.update_user_profile_fail
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    # Update mobile and send pin to verify mobile
    def update_mobile
      profile = current_user.find_or_init_profile

      if profile.verify_mobile(params[:mobile])
        render json: current_user
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(profile, false),
          status_code,
          RelatedLinksHelper.update_mobile_fail
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    def verify_mobile
      profile = current_user.profile
      # Check valid pin and valid period time
      if profile.valid_pin?(params[:pin_code])
        render json: current_user
      else
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('resources.user.invalid_pin'),
          status_code,
          RelatedLinksHelper.verify_mobile_fail
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    def favorite_partner
      fp = current_user.favorite_partner(params[:partner_id])

      if fp.save
        render json: fp
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(fp),
          status_code,
          RelatedLinksHelper.validate_favorite_partner_fail
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    def my_partners
      render json: ListPartnerSerializer.new(current_user.my_partners).as_json
    end

    # Send email to user to reset password
    def forgot_password
      user = User.where(email: params[:email]).first

      if user && user.generate_reset_password_token
        # Handle forgot worker
        unless Rails.env.test?
          noti_data = {}
          noti_data[:event] = {
            name: Constants::NotificationEvent::FORGOT_PASSWORD,
            data: { user_id: user.id }
          }
          ForgotPasswordWorker.perform_async(noti_data.to_json)
        end

        render json: { email: user.email }
      else
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('resources.user.could_not_reset_password'),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    def reset_password
      if current_user.update_password(params[:password])
        current_user.disable_reset_password_token
        render json: {}
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(current_user),
          status_code,
          RelatedLinksHelper.validate_favorite_partner_fail
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    private

    def validate_user
      @user =
        if current_user && current_user.is_a?(User) && current_user.id == params[:id].to_i
          current_user
        else
          User.where(id: params[:id]).first
        end
      return unless @user.nil?

      status_code = 404
      serialize_data = build_serialize_data(
        I18n.t('errors.messages.not_found'),
        status_code
      )
      render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
    end

    # Handle sign up params
    def sign_up_params
      { email: params[:email], password: params[:password] }
    end

    def validate_sign_up_params
      serialize_data_extra = { links: RelatedLinksHelper.sign_up_user_fail }

      validate_post_data(
        sign_up_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    # Handle sign in params
    def sign_in_params
      { email: params[:email], password: params[:password] }
    end

    def validate_sign_in_params
      serialize_data_extra = { links: RelatedLinksHelper.sign_in_user_fail }

      validate_post_data(
        sign_in_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    # Handle auth fb params
    def auth_facebook_params
      { access_token: params[:access_token] }
    end

    def validate_auth_fb_params
      serialize_data_extra = { links: RelatedLinksHelper.auth_user_fail }

      validate_post_data(
        auth_facebook_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    # Handle update mobile params
    def update_mobile_params
      { mobile: params[:mobile] }
    end

    def validate_update_mobile_params
      serialize_data_extra = { links: RelatedLinksHelper.verify_mobile_fail }

      validate_post_data(
        update_mobile_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    # Handle verify params
    def verify_mobile_params
      { pin_code: params[:pin_code] }
    end

    def validate_verify_mobile_params
      serialize_data_extra = { links: RelatedLinksHelper.verify_mobile_fail }

      validate_post_data(
        verify_mobile_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    # Handle favorite partner params
    def favorite_partner_params
      { partner_id: params[:partner_id] }
    end

    def validate_favorite_partner_params
      serialize_data_extra = { links: RelatedLinksHelper.validate_favorite_partner_fail }

      validate_post_data(
        favorite_partner_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    # Handle forgot password params
    def forgot_password_params
      { email: params[:email] }
    end

    def validate_forgot_password_params
      validate_post_data(
        forgot_password_params,
        I18n.t('errors.messages.invalid_params')
      )
    end

    # using u and access_token params
    def authenticate_reset_password_token
      if params[:u].present? && params[:t].present? && params[:password].present?
        @current_user = User.where(id: params[:u]).first
        return if current_user.present? && current_user.valid_reset_password_token?(params[:t])
      end

      status_code = 422
      serialize_data = build_serialize_data(
        I18n.t('resources.user.invalid_reset_password_token'),
        status_code,
        RelatedLinksHelper.auth_partner_fail
      )
      render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
    end
  end
end
