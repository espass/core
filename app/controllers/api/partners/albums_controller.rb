module Api
  module Partners
    class AlbumsController < ApiController
      before_action :validate_add_photos_params, only: %w[add_photos]
      before_action :validate_delete_photos_params, only: %w[delete_photos]

      before_action :authenticate_resource, except: %w[create delete add_photos delete_photos]
      before_action :authenticate_partner, only: %w[create delete add_photos delete_photos]

      before_action :validate_partner, only: %w[index]
      before_action :validate_album, only: %w[delete photos add_photos delete_photos]

      def index
        albums = @partner.albums.select(Partner::Album.base_columns)
        render json: Partner::ListAlbumSerializer.new(albums: albums, partner: @partner).to_json
      end

      def create
        album = Partner::Album.create_by_manual(params)
        album.partner_id = current_partner.id
        if album.save
          UpdateNumAlbumWorker.perform_async(current_partner.id)
          render json: album
        else
          status_code = 422
          serialize_data = build_serialize_data(
            ApplicationHelper.pretty_error(album, false),
            status_code
          )
          render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        end
      end

      def delete
        if @album.mark_delete(current_partner)
          UpdateNumAlbumWorker.perform_async(current_partner.id)
          render json: @album
        else
          status_code = 422
          serialize_data = build_serialize_data(
            ApplicationHelper.pretty_error(@album, false),
            status_code
          )
          render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        end
      end

      def photos
        render json: ListPhotoSerializer.new(@album).to_json
      end

      def add_photos
        if @album.add_photos(current_partner, params[:photos])
          render json: @album
        else
          status_code = 422
          serialize_data = build_serialize_data(
            ApplicationHelper.pretty_error(@album, false),
            status_code
          )
          render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        end
      end

      def delete_photos
        if @album.delete_photos(current_partner, params[:photo_ids])
          render json: @album
        else
          status_code = 422
          serialize_data = build_serialize_data(
            ApplicationHelper.pretty_error(@album, false),
            status_code,
            RelatedLinksHelper.create_media_fail
          )
          render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        end
      end

      private

      def validate_album
        @album = Partner::Album.where(
          id: params[:ab_id], is_deleted: false
        ).first
        return unless @album.nil?

        status_code = 404
        serialize_data = build_serialize_data(
          I18n.t('errors.messages.not_found'),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end

      # Handle add photos params
      def add_photos_params
        return { photos: nil } unless params[:photos].is_a?(Array)
        { photos: params[:photos] }
      end

      def validate_add_photos_params
        validate_post_data(
          add_photos_params,
          I18n.t('errors.messages.invalid_params')
        )
      end

      # Handle delete photos params
      def delete_photos_params
        return { photo_ids: nil } unless params[:photo_ids].is_a?(Array)
        { photo_ids: params[:photo_ids] }
      end

      def validate_delete_photos_params
        validate_post_data(
          delete_photos_params,
          I18n.t('errors.messages.invalid_params')
        )
      end
    end
  end
end
