module Api
  module Partners
    class PhotosController < ApiController
      before_action :authenticate_partner
      before_action :validate_photo, only: %w[]

      private

      def validate_photo
        @photo = Partner::Photo.where(
          id: params[:photo_id], is_deleted: false
        ).first
        return unless @photo.nil?

        status_code = 404
        serialize_data = build_serialize_data(
          I18n.t('errors.messages.not_found'),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end
  end
end
