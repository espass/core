module Api
  module Partners
    class ReviewsController < ApiController
      before_action :authenticate_resource, except: %w[create]
      before_action :authenticate_user, only: %w[create]

      before_action :validate_partner, only: %w[index]
      before_action :user_reviewable, only: %w[create]

      def index
        reviews = @partner.reviews.includes(:user_profile).page(current_page).select(
          Partner::Review.base_columns
        )
        pagination = load_pagination_data(reviews)

        render json: Partner::ListReviewSerializer.new(
          reviews: reviews, partner: @partner, pagination: pagination
        ).to_json
      end

      def create
        review = Partner::Review.create_by_manual(params)
        review.user_id = current_user.id
        review.partner_id = params[:id]

        if review.save
          render json: review
        else
          status_code = 422
          serialize_data = build_serialize_data(
            ApplicationHelper.pretty_error(review, false),
            status_code
          )
          render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        end
      end

      private

      def user_reviewable
        reviewable = Order.where(
          user_id: current_user.id, id: params[:order_id],
          partner_id: params[:id],
          status: Constants::OrderStatus::SUCCESS
        ).count.nonzero?
        return if reviewable

        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('resources.partner.review.could_not_review'),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end
  end
end
