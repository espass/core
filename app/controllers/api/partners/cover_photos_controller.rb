module Api
  module Partners
    class CoverPhotosController < ApiController
      before_action :authenticate_partner
      before_action :validate_partner, only: %w[create]

      def create
        cover_photo = Partner::CoverPhoto.create_by_manual(params, current_partner)

        if cover_photo.save
          render json: cover_photo
        else
          status_code = 422
          serialize_data = build_serialize_data(
            ApplicationHelper.pretty_error(cover_photo, false),
            status_code
          )
          render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        end
      end
    end
  end
end
