module Api
  module Partners
    class WorkingTimesController < ApiController
      before_action :validate_create_params, only: %w[create]
      before_action :authenticate_partner

      before_action :validate_working_time, only: %w[update delete]

      def index
        render json: Partner::ListWorkingTimeSerializer.new(
          working_times: current_partner.working_times,
          partner: current_partner
        ).as_json
      end

      def create
        working_time = Partner::WorkingTime.new(create_params)
        working_time.partner_id = @current_partner.id
        if working_time.save
          UpdatePartnerIndexWorker.perform_async(current_partner.id)
        end

        render json: working_time
      end

      def update
        if @working_time.update_by_manual(params)
          UpdatePartnerIndexWorker.perform_async(current_partner.id)
          render json: @working_time
        else
          status_code = 422
          serialize_data = build_serialize_data(
            ApplicationHelper.pretty_error(@working_time),
            status_code
          )
          render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        end
      end

      def delete
        @working_time.delete
        UpdatePartnerIndexWorker.perform_async(current_partner.id)

        render json: @working_time
      end

      def today
        render json: Partner::ListWorkingTimeSerializer.new(
          working_times: current_partner.working_today_time,
          partner: current_partner
        ).as_json
      end

      private

      def validate_working_time
        @working_time = Partner::WorkingTime.where(id: params[:wt_id], partner_id: current_partner.id).first
        return unless @working_time.nil?

        status_code = 404
        serialize_data = build_serialize_data(
          I18n.t('errors.messages.not_found'),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end

      # Handle create params
      def create_params
        return { valid: nil } unless Partner::WorkingTime.duration_valid?(params[:from])
        return { valid: nil } unless Partner::WorkingTime.duration_valid?(params[:to])
        return { valid: nil } unless Partner::WorkingTime.repeat_valid?(params[:repeat])

        { from: params[:from], to: params[:to], repeat: params[:repeat] }
      end

      def validate_create_params
        validate_post_data(
          create_params,
          I18n.t('errors.messages.invalid_params')
        )
      end
    end
  end
end
