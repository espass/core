module Api
  class ServicesController < ApiController
    before_action :authenticate_resource

    def index
      render json: ListServiceSerializer.new(services: BaseService.all).to_json
    end
  end
end
