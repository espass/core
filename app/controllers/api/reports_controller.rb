module Api
  class ReportsController < ApiController
    before_action :authenticate_partner, only: %w[earning]

    def earning
      time_condition = period_of_time
      earning = load_earning_detail(time_condition)
      render json: EarningSerializer.new(earning, time_condition).to_json
    end

    private

    def period_of_time
      time_condition = {}
      time_condition[:from] = params[:from].blank? ? Time.now.beginning_of_day - 7.days : params[:from]
      time_condition[:to] = params[:to].blank? ? Time.now : params[:to]
      time_condition
    end

    def load_earning_detail(time_condition)
      Order.where(
        'partner_id = ? AND status = ? AND finished_at > ? AND finished_at < ?',
        current_partner.id,
        Constants::OrderStatus::SUCCESS,
        time_condition[:from],
        time_condition[:to]
      ).select(Order.report_columns).to_a
    end
  end
end
