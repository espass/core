module Api
  class NotificationsController < ApiController
    before_action :authenticate_resource
    before_action :validate_notification, only: %w[delete mark_read]
    before_action :set_current_resource, only: %w[index]

    def index
      notifications = @current_resource.my_notifications.order(id: :desc).page(current_page)
      pagination = load_pagination_data(notifications)

      render json: ListNotificationSerializer.new(
        notifications: notifications, pagination: pagination
      ).to_json
    end

    def delete
      if @notification.mark_delete(@current_resource)
        render json: @notification
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(@notification, false),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    def mark_read
      if @current_resource.read_notification(@notification)
        head :ok
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(@current_resource, false),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    private

    def validate_notification
      @notification = Notification.where(id: params[:id]).first
      return unless @notification.nil?

      status_code = 404
      serialize_data = build_serialize_data(
        I18n.t('errors.messages.not_found'),
        status_code
      )
      render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
    end
  end
end
