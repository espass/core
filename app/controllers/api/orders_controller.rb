module Api
  class OrdersController < ApiController
    before_action :validate_create_order_quickly_params, only: %w[create_quickly]
    before_action :validate_create_order_params, only: %w[create]

    before_action :authenticate_resource, only: %w[cancel index history scheduled detail]
    before_action :authenticate_user, only: %w[create create_quickly]
    before_action :authenticate_partner, only: %w[
      coming_soon accept decline finish partner_process
    ]

    before_action :validate_order, only: %w[accept decline cancel finish partner_process detail]
    before_action :validate_order_ownership, only: %w[detail]

    def index
      orders = @current_resource.my_orders.page(current_page).includes(:partner_profile)
      pagination = load_pagination_data(orders)
      links = { self: request.path }

      response =
        if @current_resource.is_a?(Partner)
          Partner::ListOrderSerializer.new(orders: orders, pagination: pagination, links: links).to_json
        else
          User::ListOrderSerializer.new(orders: orders, pagination: pagination, links: links).to_json
        end
      render json: response
    end

    def coming_soon
      orders = current_partner.coming_soon_orders.page(current_page).includes(:user_profile)
      pagination = load_pagination_data(orders)
      links = { self: request.path }

      render json: Partner::ListOrderSerializer.new(
        orders: orders, pagination: pagination, links: links
      ).to_json
    end

    def history
      orders = @current_resource.my_order_history.page(current_page)
      pagination = load_pagination_data(orders)
      links = { self: request.path }

      response =
        if @current_resource.is_a?(Partner)
          Partner::ListOrderSerializer.new(orders: orders, pagination: pagination, links: links).to_json
        else
          User::ListOrderSerializer.new(orders: orders, pagination: pagination, links: links).to_json
        end
      render json: response
    end

    def scheduled
      orders = @current_resource.scheduled_orders.page(current_page)
      pagination = load_pagination_data(orders)
      links = { self: request.path }

      response =
        if @current_resource.is_a?(Partner)
          Partner::ListOrderSerializer.new(orders: orders, pagination: pagination, links: links).to_json
        else
          User::ListOrderSerializer.new(orders: orders, pagination: pagination, links: links).to_json
        end
      render json: response
    end

    def create
      order_service = OrderServices.new(params.as_json)
      order = order_service.create(current_user)

      render json: BookingSerializer.new(order).to_json
    rescue Edela::OrderError::FailedToCreate => e
      status_code = 422
      serialize_data = build_serialize_data(
        e.message, status_code,
        RelatedLinksHelper.create_order_fail
      )
      render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
    end

    def create_quickly
      order_service = OrderServices.new(params.as_json)
      order = order_service.create_quickly(current_user)

      render json: QuickBookingSerializer.new(order).to_json
    rescue Edela::OrderError::FailedToCreate => e
      status_code = 422
      serialize_data = build_serialize_data(
        e.message, status_code,
        RelatedLinksHelper.create_order_fail
      )
      render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
    end

    # Only for the partner
    def accept
      # Steps:
      # Find the order
      # Validate the order: status
      # Validate the partner (busy, prevent spamming)
      # IMPORTANT: check the order assigned to this partner?
      # Update status, level
      # Send and create notification for user
      unless @order.acceptable?(current_partner)
        # Can't accept this order
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('resources.order.could_not_accept'), status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        return
      end

      unless @order.partner_accepted(@order.booking_line)
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(@order),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        return
      end

      render json: Order::PreviewSerializer.new(@order).as_json
    end

    def decline
      # Steps
      # Find the order
      # Validate the order
      # Save history declined
      # Find other partner & save last notification
      unless @order.declinable?(current_partner)
        # Can't decline this order
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('resources.order.could_not_decline'), status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        return
      end

      @order.partner_decline(decline_params)

      render json: Order::PreviewSerializer.new(@order).as_json
    end

    def partner_process
      # Steps:
      # Update order status
      # Update level
      # Noti for user
      unless @order.processable?(current_partner)
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('resources.order.could_not_process'), status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        return
      end

      unless @order.process
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(@order),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        return
      end

      render json: Order::PreviewSerializer.new(@order).as_json
    end

    # Supported transaction method: cash
    def finish
      # Input params: amount, ...
      # Steps:
      # + Update order status
      # + Update transaction
      # + Push notification for user
      unless @order.finishable?(current_partner)
        # Can't finish this order
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('resources.order.could_not_finish'), status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        return
      end

      unless @order.finish(params)
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(@order),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        return
      end

      render json: Order::PreviewSerializer.new(@order).as_json
    end

    def cancel
      # Steps
      # Find the order
      # Validate order: cancel time, reason,...
      # Validate resource
      # Update status, level, who cancel
      # Push notification
      unless @order.cancelable?(@current_resource)
        # Can't cancel this order
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('resources.order.could_not_cancel'), status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        return
      end

      unless @order.cancel(@current_resource)
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(@order),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
        return
      end

      render json: Order::PreviewSerializer.new(@order).as_json
    end

    def detail
      response =
        if @current_resource.is_a?(Partner)
          Partner::OrderSerializer.new(@order).to_json
        else
          User::OrderSerializer.new(@order).to_json
        end

      render json: response
    end

    private

    def validate_order
      @order = Order.where(id: params[:id]).first
      return unless @order.nil?

      status_code = 404
      serialize_data = build_serialize_data(
        I18n.t('errors.messages.not_found'),
        status_code
      )
      render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
    end

    def validate_order_ownership
      return if @order.owner?(@current_resource)

      status_code = 422
      serialize_data = build_serialize_data(
        I18n.t('errors.messages.no_permission'),
        status_code
      )
      render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
    end

    # Required params
    def create_order_params
      {
        partner_id: params[:partner_id],
        price: params[:price], booking_time: params[:booking_time],
        location: params[:location], address: params[:address],
        services: params[:services]
      }
    end

    def validate_create_order_params
      serialize_data_extra = { links: RelatedLinksHelper.create_order_fail }

      validate_post_data(
        create_order_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    # Required params
    def create_order_quickly_params
      {
        booking_time: params[:booking_time],
        location: params[:location], address: params[:address],
        base_services: params[:base_services]
      }
    end

    def validate_create_order_quickly_params
      serialize_data_extra = { links: RelatedLinksHelper.create_order_fail }

      validate_post_data(
        create_order_quickly_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    def decline_params
      params.permit(:reason)
    end
  end
end
