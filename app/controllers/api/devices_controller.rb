module Api
  class DevicesController < ApiController
    before_action :validate_register_params, only: %w[register]

    # Params: os, os_version, lang, device_id, device_token
    # Required params: os, os_version, lang, device_id
    def register
      device = GlobalDevice.find_or_initialize_by(device_id: params[:device_id])
      device.update_info(params)

      render json: device
    end

    private

    # Handle sign up params
    def register_params
      {
        os: params[:os], os_version: params[:os_version],
        lang: params[:lang], device_id: params[:device_id]
      }
    end

    def validate_register_params
      serialize_data_extra = { links: RelatedLinksHelper.register_device_fail }

      validate_post_data(
        register_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end
  end
end
