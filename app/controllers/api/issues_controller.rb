module Api
  class IssuesController < ApiController
    before_action :authenticate_resource

    def create
      issue = Issue.create_by_manual(params, @current_resource)

      if issue.save
        render json: issue
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(issue, false),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end
  end
end
