module Api
  class SettingsController < ApiController
    before_action :authenticate_resource, only: %w[price_list]

    def index
      settings =
        if params[:app_name] == Constants::AppName::USER
          Rails.cache.fetch('user_settings', expires_in: 30.days) do
            User::SettingSerializer.new('').to_json
          end
        else
          Rails.cache.fetch('partner_settings', expires_in: 30.days) do
            Partner::SettingSerializer.new('').to_json
          end
        end

      render json: settings
    end

    def price_list
      render json: File.read('resources/settings/price_list.json')
    end
  end
end
