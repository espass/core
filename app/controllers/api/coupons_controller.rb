module Api
  class CouponsController < ApiController
    before_action :validate_check_params, only: %w[check]
    before_action :authenticate_user
    before_action :validate_coupon, only: %w[check]

    def check
      if @coupon.expired?
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('resources.coupon.invalid'),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      else
        render json: @coupon
      end
    end

    private

    def validate_coupon
      @coupon = Coupon.where(code: params[:coupon_code]).first
      return unless @coupon.nil?

      status_code = 422
      serialize_data = build_serialize_data(
        I18n.t('resources.coupon.invalid'),
        status_code
      )
      render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
    end

    # Handle sign up params
    def check_params
      { coupon_code: params[:coupon_code] }
    end

    def validate_check_params
      validate_post_data(
        check_params,
        I18n.t('errors.messages.invalid_params')
      )
    end
  end
end
