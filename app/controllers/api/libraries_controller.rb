module Api
  class LibrariesController < ApiController
    before_action :authenticate_resource
    before_action :validate_create_params, only: %w[create]
    before_action :validate_media_file, only: %w[create]
    before_action :validate_library, only: %w[delete]

    def create
      media_content = params[:media].read
      old_media_name = params[:media].original_filename
      media_key = LibraryHelper.generate_key(@current_resource, old_media_name)
      LibraryHelper.put_object_to_s3(media_key, media_content)

      library = Library.create_by_manual(media_key, @current_resource)
      render json: library
    end

    def delete
      if @library.mark_delete(@current_resource)
        render json: @library
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(@library, false),
          status_code,
          RelatedLinksHelper.delete_media_fail
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    private

    def validate_library
      @library = Library.where(id: params[:id], is_deleted: false).first
      return unless @library.nil?

      status_code = 404
      serialize_data = build_serialize_data(
        I18n.t('errors.messages.not_found'),
        status_code
      )
      render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
    end

    # Handle sign up params
    def create_params
      { media: params[:media] }
    end

    def validate_media_file
      return if LibraryHelper.bytes_to_mbs(params[:media].size) < 3

      status_code = 422
      serialize_data = build_serialize_data(
        I18n.t('resources.library.invalid_media_size'),
        status_code,
        RelatedLinksHelper.create_media_fail
      )
      render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
    end

    def validate_create_params
      serialize_data_extra = { links: RelatedLinksHelper.create_media_fail }

      validate_post_data(
        create_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end
  end
end
