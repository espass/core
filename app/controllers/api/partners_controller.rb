module Api
  class PartnersController < ApiController
    before_action :validate_filter_params, only: %w[index]
    before_action :validate_search_params, only: %w[search]
    before_action :validate_sign_in_params, only: %w[sign_in]
    before_action :validate_update_services_params, only: %w[update_services]
    before_action :validate_nearby_params, only: %w[nearby]

    before_action :authenticate_resource, except: %w[sign_in sign_out update_profile]
    before_action :authenticate_partner, only: %w[sign_out update_profile ping]
    before_action :validate_partner, only: %w[profile update_profile update_services]

    before_action :set_current_user, only: %w[profile]

    # Params: location, order_time, services
    def index
      partner_ids = PartnerServices.filter(params[:location], params[:order_time], params[:services])

      partners = Partner.where(id: partner_ids).includes(:profile_only_fetch_preview)
      render json: FilterSerializer.new(partners).as_json
    end

    # params: q (keyword)
    def search
      partner_ids = PartnerServices.search(params[:q])

      partners = Partner.where(id: partner_ids).includes(:profile_only_fetch_preview)
      render json: FilterSerializer.new(partners).as_json
    end

    # Require location
    def nearby
      partner_ids = PartnerServices.nearby(params[:location])

      partners = Partner.where(id: partner_ids).includes(:profile_only_fetch_preview)
      render json: FilterSerializer.new(partners).as_json
    end

    def profile
      render json: @partner
    end

    # Required params: email, password
    def sign_in
      partner = Partner.where(email: params[:email]).includes(:profile_only_fetch_auth).first
      serialize_data = { links: { self: sign_in_partners_path } }

      if partner && partner.authenticate(params[:password])
        # Refresh token, set authen information and auto save
        partner.refresh_token(extra_authen_params, true)

        serialize_data[:resource] = partner
        render json: AuthSerializer.new(serialize_data).to_json
      else
        status_code = 422
        serialize_data = build_serialize_data(
          I18n.t('errors.messages.invalid_email_or_password'),
          status_code,
          RelatedLinksHelper.sign_in_partner_fail
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    # Required params: auth_token
    def sign_out
      current_partner.refresh_token({}, true)

      serialize_data = {
        links: { self: sign_in_partners_path },
        resource: current_partner
      }

      render json: AuthSerializer.new(serialize_data).to_json
    end

    def ping
      render nothing: true
    end

    def update_profile
      if current_partner.update_profile(params)
        UpdatePartnerIndexWorker.perform_async(current_partner.id)
        render json: current_partner.profile
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(current_partner, false),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    def update_services
      if current_partner.update_services(update_services_params)
        UpdatePartnerIndexWorker.perform_async(current_partner.id)
        render json: Partner::ListServiceSerializer.new(services: current_partner.services).to_json
      else
        status_code = 422
        serialize_data = build_serialize_data(
          ApplicationHelper.pretty_error(current_partner, false),
          status_code
        )
        render json: ErrorSerializer.new(serialize_data).to_json, status: status_code
      end
    end

    private

    # Handle filter params
    def filter_params
      { location: params[:location], order_time: params[:order_time], services: params[:services] }
    end

    def validate_filter_params
      serialize_data_extra = { links: RelatedLinksHelper.filter_fail }

      validate_post_data(
        filter_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    # Handle search params
    def search_params
      { q: params[:q] }
    end

    def validate_search_params
      serialize_data_extra = { links: RelatedLinksHelper.search_fail }

      validate_post_data(
        search_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    # Handle sign in params
    def sign_in_params
      { email: params[:email], password: params[:password] }
    end

    def validate_sign_in_params
      serialize_data_extra = { links: RelatedLinksHelper.sign_in_partner_fail }

      validate_post_data(
        sign_in_params,
        I18n.t('errors.messages.invalid_params'),
        serialize_data_extra
      )
    end

    # Handle update services params
    def update_services_params
      return { services: nil } unless params[:services].is_a?(Array)
      { services: params[:services] }
    end

    def validate_update_services_params
      validate_post_data(
        update_services_params,
        I18n.t('errors.messages.invalid_params')
      )
    end

    def nearby_params
      { location: params[:location] }
    end

    def validate_nearby_params
      validate_post_data(
        nearby_params,
        I18n.t('errors.messages.invalid_params')
      )
    end
  end
end
