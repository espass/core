class PartnerServices
  class << self
    INDEX_NAME = 'core'.freeze
    PARTNER_TYPE = 'partner'.freeze
    ORDER_TYPE = 'order'.freeze

    def filter(location, order_time, service_ids)
      query = build_query_by_manual(location, order_time, service_ids)
      looking_partner_by_direct_condition(query)
    end

    def search(keyword, location = ENV['LOCATION_DEFAULT'])
      query = build_query_for_search(keyword, location)
      looking_partner_by_direct_condition(query)
    end

    def quick_looking(order)
      # Looking for partner by order
      query = build_query_for_filter_base_on_order(order)
      partner_ids = looking_for_partner_by_order(query)
      return partner_ids if partner_ids.present?

      # Looking for partner by location
      query = build_query_for_filter_base_on_partner(order)
      looking_partner_by_direct_condition(query)
    end

    def nearby(location)
      query = build_query_for_nearby(location)
      looking_partner_by_direct_condition(query)
    end

    private

    def looking_partner_by_direct_condition(query)
      filter_results = ES_CLIENT.search(index: INDEX_NAME, type: PARTNER_TYPE, body: query)
      sources = ElasticsearchHelper.load_source(filter_results)

      sources.map do |source|
        source['partner_id'].to_i
      end
    rescue
      []
    end

    def looking_for_partner_by_order(query)
      filter_results = ES_CLIENT.search(index: INDEX_NAME, type: ORDER_TYPE, body: query)
      sources = ElasticsearchHelper.load_source(filter_results)

      sources.map do |source|
        source['partner_id'].to_i
      end
    rescue
      []
    end

    # Build elasticsearch query to find the partner based on order condition:
    # The partner must valid working times and supported services that customer wants
    # The partner is doing the order with 5km distance compared to the location
    #   of the customer who wants to make up
    # The partner is doing the order that period of time valid to do new order
    def build_query_for_filter_base_on_order(order)
      filter_clauses = []
      # Filter partner condition
      filter_clauses << build_join_partner_on_order_query(order)
      # Order distance condition
      filter_clauses << ElasticsearchHelper.build_query_for_filter_location(order.location, '5km')
      # Booking time condition
      filter_clauses << build_order_condition_by_booking_time(order.booking_time)

      {
        query: {
          bool: {
            filter: filter_clauses
          }
        },
        sort: sort_by_distance(order.location)
      }
    end

    def build_query_for_filter_base_on_partner(order)
      filter = []
      filter << ElasticsearchHelper.build_query_for_filter_location(order.location)
      filter << ElasticsearchHelper.build_query_for_filter_services(order.base_services)
      filter << ElasticsearchHelper.build_query_for_filter_working_times(order.booking_time)

      must_not = build_join_order_on_partner_query(order.booking_time)

      {
        query: {
          bool: {
            filter: filter,
            must_not: must_not
          }
        },
        sort: sort_by_distance(order.location),
        _source: ['partner_id']
      }
    end

    def build_query_by_manual(location, order_time, service_ids)
      filter = []
      filter << ElasticsearchHelper.build_query_for_filter_location(location)

      must = []
      must << ElasticsearchHelper.build_query_for_filter_services(service_ids)
      must << ElasticsearchHelper.build_query_for_filter_working_times(order_time)

      {
        query: {
          bool: {
            filter: filter,
            must: must
          }
        },
        sort: sort_by_distance(location),
        _source: ['partner_id']
      }
    end

    def build_query_for_nearby(location)
      filter = []
      filter << ElasticsearchHelper.build_query_for_filter_location(location, '5km')

      {
        query: {
          bool: {
            filter: filter
          }
        }
      }
    end

    def build_query_for_search(keyword, location)
      {
        query: query_multi_match(keyword),
        sort: sort_by_distance(location),
        _source: ['partner_id']
      }
    end

    def query_multi_match(keyword)
      {
        multi_match: {
          query: keyword,
          fields: %w[name address service_names]
        }
      }
    end

    def sort_by_distance(location, order_by = 'asc')
      {
        _geo_distance: {
          location: location,
          order: order_by
        }
      }
    end

    def build_join_partner_on_order_query(order)
      {
        has_parent: {
          type: 'partner',
          query: {
            bool: {
              filter: [
                ElasticsearchHelper.build_query_for_filter_services(order.base_services),
                ElasticsearchHelper.build_query_for_filter_working_times(order.booking_time)
              ]
            }
          }
        }
      }
    end

    def build_join_order_on_partner_query(booking_time)
      {
        has_child: {
          type: 'order',
          query: {
            bool: {
              filter: {
                range: {
                  booking_time: {
                    gte: booking_time - 1.5.hours,
                    lte: booking_time + 1.5.hours
                  }
                }
              }
            }
          }
        }
      }
    end

    def build_order_condition_by_booking_time(booking_time)
      {
        bool: {
          should: [
            {
              range: {
                booking_time: {
                  gte: booking_time - 2.hours,
                  lte: booking_time - 1.5.hours
                }
              }
            },
            {
              range: {
                booking_time: {
                  gte: booking_time + 1.5.hours,
                  lte: booking_time + 2.hours
                }
              }
            }
          ]
        }
      }
    end
  end
end
