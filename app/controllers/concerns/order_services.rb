class OrderServices
  attr_reader :params
  attr_reader :current_user

  def initialize(params)
    raise 'Only support: Hash' unless params.is_a?(Hash)

    @params = params
  end

  def create(current_user)
    @current_user = current_user
    order = Order.new(order_params)
    raise Edela::OrderError::FailedToCreate.new, ApplicationHelper.pretty_error(order) unless order.save
    # Create order services for order
    create_order_services(order, params['services'])

    # Request partner
    order.request_partner

    order
  rescue ActiveRecord::Rollback => e
    # Rollback if create services failed
    order.rollback

    raise Edela::OrderError::FailedToCreate.new, e.message
  end

  def create_quickly(current_user)
    @current_user = current_user
    order = Order.new(order_quickly_params)
    order = fill_coupon_data_up(order) if order.coupon_code.present?
    raise Edela::OrderError::FailedToCreate.new, ApplicationHelper.pretty_error(order) unless order.save

    # Request partner
    order.request_partner
    order
  rescue ActiveRecord::Rollback => e
    # Rollback if create services failed
    order.rollback if order.present?

    raise Edela::OrderError::FailedToCreate.new, e.message
  end

  private

  def order_params
    {
      partner_id: params['partner_id'], price: params['price'],
      booking_time: params['booking_time'],
      location: params['location'], address: params['address'],
      user_id: current_user.id, base_services: params['base_services']
    }
  end

  def create_order_services(order, services)
    unless services.is_a?(Array)
      raise ActiveRecord::Rollback, I18n.t('internal.errors.messages.wrong_format_hash', field: 'services')
    end

    services.each do |serv_data|
      serv = Order::Service.new
      serv.partner_service_id = serv_data['partner_service_id']
      serv.price = serv_data['price']
      serv.order_id = order.id

      next if serv.save

      # Something unusual when creating order service
      raise ActiveRecord::Rollback, ApplicationHelper.pretty_error(serv)
    end
  end

  def order_quickly_params
    %w[attachments base_services].each do |field|
      next if params[field].is_a?(Array)
      raise ActiveRecord::Rollback, I18n.t('internal.errors.messages.wrong_format', field: field)
    end

    {
      booking_time: params['booking_time'], user_id: current_user.id,
      location: params['location'], address: params['address'],
      attachments: params['attachments'],
      base_services: params['base_services'],
      booking_type: Constants::BookingType::QUICKLY,
      status: Constants::OrderStatus::REQUESTED,
      coupon_code: params['coupon_code']
    }
  end

  def fill_coupon_data_up(order)
    coupon = Coupon.where(code: order.coupon_code).first
    if coupon.nil? || coupon.expired?
      order.coupon_code = nil
    else
      order.coupon_discount = coupon.discount
    end

    order
  end
end
