class ElasticsearchServices
  class OrderIndex
    attr_reader :index_name
    attr_reader :type_name

    def initialize(index_name = 'core', type_name = 'order')
      @index_name = index_name
      @type_name = type_name
    end

    def insert_document(order)
      return if order.partner_id.nil?

      index_data = {}
      index_data[:location] = order.location
      index_data[:booking_time] = order.booking_time
      index_data[:partner_id] = order.partner_id
      # Insert using es client
      ES_CLIENT.index(
        index: index_name, type: type_name, id: order.id,
        body: index_data, parent: order.partner_id
      )
    end

    def delete_document(order)
      return if order.partner_id.nil?
      ES_CLIENT.delete(index: index_name, type: type_name, id: order.id, parent: order.partner_id)
    end
  end
end
