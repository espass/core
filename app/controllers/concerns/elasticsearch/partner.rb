class ElasticsearchServices
  class PartnerIndex
    attr_reader :index_name
    attr_reader :type_name

    def initialize(index_name = 'core', type_name = 'partner')
      @index_name = index_name
      @type_name = type_name
    end

    def insert_document(partner)
      profile = partner.profile
      services = partner.services
      working_times = partner.working_times.where(enabled: true)

      index_data = {}
      index_data[:partner_id] = partner.id
      index_data.merge!(profile.as_json(only: %w[name address location])) if profile.present?
      index_data[:services] = services.as_json(only: %w[service_id name]) if services.present?
      if working_times.present?
        index_data[:working_times] = working_times.map do |wt|
          { from: wt.from, to: wt.to, repeat: wt.repeat, enabled: wt.enabled }
        end
      end

      # Insert using es client
      ES_CLIENT.index(index: index_name, type: type_name, id: partner.id, body: index_data)
    end
  end
end
