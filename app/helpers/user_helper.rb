module UserHelper
  class << self
    def base_profile_field
      %w[name address location]
    end
  end
end
