module RelatedLinksHelper
  class << self
    include Rails.application.routes.url_helpers

    def auth_user_fail
      {
        sign_in: sign_in_users_path,
        sign_up: sign_up_users_path
      }
    end

    def auth_partner_fail
      {
        sign_in: sign_in_users_path,
        sign_up: sign_up_users_path
      }
    end

    def sign_in_user_fail
      {
        sign_up: sign_up_users_path
      }
    end

    def sign_in_partner_fail
      {}
    end

    def sign_up_user_fail
      {
        sign_in: sign_in_users_path
      }
    end

    def filter_fail
      {}
    end

    def search_fail
      {}
    end

    def update_user_profile_fail
      {}
    end

    def register_device_fail
      {}
    end

    def create_order_fail
      {}
    end

    def create_media_fail
      {}
    end

    def delete_media_fail
      {}
    end

    def update_mobile_fail
      {}
    end

    def verify_mobile_fail
      {}
    end

    def validate_favorite_partner_fail
      {}
    end

    def validate_my_partners_fail
      {}
    end
  end
end
