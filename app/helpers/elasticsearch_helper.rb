module ElasticsearchHelper
  class << self
    def load_source(es_result)
      es_result['hits']['hits'].map { |item| item['_source'] }
    end

    def build_query_for_filter_location(location, default_distance_limited = '30km')
      return {} unless location_valid?(location)

      {
        geo_distance: {
          distance: default_distance_limited,
          location: location
        }
      }
    end

    def build_query_for_filter_services(service_ids)
      return {} unless service_ids.is_a?(Array)

      {
        nested: {
          path: 'services',
          query: {
            bool: {
              must: [
                terms: { 'services.service_id' => service_ids }
              ]
            }
          }
        }
      }
    end

    def build_query_for_filter_working_times(booking_time)
      booking_time = Time.zone.parse(booking_time) unless booking_time.is_a?(Time)
      duration = booking_time.hour * 3600 + booking_time.min * 60 + booking_time.sec
      wday = booking_time.wday

      {
        nested: {
          path: 'working_times',
          query: {
            bool: {
              must: [
                { term: { 'working_times.repeat' => wday } },
                { range: { 'working_times.from' => { lte: duration } } },
                { range: { 'working_times.to' => { gte: duration } } }
              ]
            }
          }
        }
      }
    end

    private

    def location_valid?(location)
      location.match(/^[\-0-9\.]+,[\ \-0-9\.]+$/).present?
    end
  end
end
