module LibraryHelper
  class << self
    def put_object_to_s3(key, content, object_acl = 'public-read')
      MEDIA_BUCKET.put_object(
        key: key,
        body: content,
        acl: object_acl
      )
    end

    def bytes_to_mbs(bytes)
      bytes / 1048576.0
    end

    def generate_key(current_resource, old_media_name)
      media_type = File.extname(old_media_name)
      media_name = Base64.urlsafe_encode64(old_media_name) + '-e' + media_type

      timestamp = Time.now.to_f.to_s.remove('.').to_i
      prefix = "m/f/#{timestamp}/#{timestamp + current_resource.id}_"

      prefix + media_name
    end
  end
end
