module ApplicationHelper
  def self.pretty_error(object, show_key = true)
    error = object.errors.messages.first
    message = ''

    if error.present?
      message << "#{error[0]} " if show_key
      message << error[1].join(' ')
    end

    message
  end

  def self.time_to_duration(time)
    t = Time.parse(time)

    t.hour * 3600 + t.min * 60 + t.sec
  end

  def self.data_default
    {
      avatar: "#{ENV['AWS_CF_MEDIA']}/default/avatar.png",
      cover_photo: "#{ENV['AWS_CF_MEDIA']}/default/cover_photo.png",
      album: "#{ENV['AWS_CF_MEDIA']}/default/album.png"
    }
  end
end
