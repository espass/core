class PreventSpam
  include Rails.application.routes.url_helpers

  def initialize(app)
    @app = app
  end

  def call(env)
    if env['REQUEST_PATH'] == create_quickly_orders_path && booking_valid?(load_params(env)) == false
      headers = { 'Content-Type' => 'application/json; charset=utf-8' }
      status_code = 403
      serialize_data = { code: status_code, message: I18n.t('resources.order.could_not_book') }

      Rack::Response.new(ErrorSerializer.new(serialize_data).to_json, status_code, headers)
    else
      @app.call(env)
    end
  end

  private

  def booking_valid?(params)
    auth_token = params['auth_token']
    booking_time = params['booking_time']
    return false if auth_token.blank?
    # Connect to redis server with namespace is 6
    redis = Redis.new(host: ENV['REDIS_HOST'], db: 6)
    # load booking history to filter
    booking_histories = redis.lrange(auth_token, 0, -1)
    if booking_histories.blank?
      create_booking_histories_key(redis, auth_token, booking_time)
      return true
    end

    return false if booking_time == booking_histories.last || booking_histories.count > 1
    redis.rpush(auth_token, booking_time)
  rescue StandardError
    true
  end

  def create_booking_histories_key(redis, auth_token, booking_time)
    redis.rpush(auth_token, booking_time)
    redis.expire(auth_token, 30)
  end

  def load_params(env)
    request = Rack::Request.new(env)
    if env['CONTENT_TYPE'] == 'application/json'
      content = env['rack.input'].read
      env.update('rack.input' => StringIO.new(content))

      params = JSON.parse(content)
    else
      params = request.params
    end

    params
  end
end
