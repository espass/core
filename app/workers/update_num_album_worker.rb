class UpdateNumAlbumWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, backtrace: true
  sidekiq_options retry: 1

  def perform(partner_id)
    num_album = Partner::Album.where(is_deleted: false, partner_id: partner_id).count
    Partner::Profile.where(partner_id: partner_id).update(num_album: num_album)
  end
end
