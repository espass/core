class UpdatePartnerIndexWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, backtrace: true
  sidekiq_options retry: 1

  def perform(partner_id)
    partner = Partner.where(id: partner_id).first
    return if partner.nil?
    es = ElasticsearchServices::PartnerIndex.new
    es.insert_document(partner)
  end
end
