class UpdateOrderIndexWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, backtrace: true
  sidekiq_options retry: 1

  def perform(order_id)
    order = Order.where(id: order_id).first
    return if order.nil? || order.partner_id.nil?
    es = ElasticsearchServices::OrderIndex.new

    if [Constants::OrderStatus::PENDING, Constants::OrderStatus::PROCESSING].include?(order.status)
      es.insert_document(order)
    else
      es.delete_document(order)
    end
  end
end
