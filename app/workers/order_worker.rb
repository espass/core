class OrderWorker
  attr_reader :order, :event_data, :event_name
  include Shoryuken::Worker

  shoryuken_options queue: ENV['ORDER_WORKER_QUEUE'], auto_delete: true

  def perform(_sqs_msg, message)
    # Message schema: { event: 'event', data: { order_id: '' } }
    event = JSON.parse(message)
    @event_name = event['event']['name']
    @event_data = event['event']['data']
    @order = load_order
    return if @order.nil?

    case event_name
    when Constants::NotificationEvent::ACCEPTED_ORDER
      accepted_order_handler
    when Constants::NotificationEvent::REQUEST_PARTNER
      request_partner_handler
    when Constants::NotificationEvent::CANCELED_ORDER
      canceled_order_handler
    when Constants::NotificationEvent::PROCESS_ORDER
      process_order_handler
    when Constants::NotificationEvent::FINISHED_ORDER
      finished_order_handler
    when Constants::NotificationEvent::NO_PARTNER
      no_partner_for_order_handler
    end
  end

  private

  def accepted_order_handler
    user = User.joins(:device).where(id: order.user_id).includes(:device).first
    partner_profile = Partner::Profile.where(partner_id: order.partner_id).select(:name, :avatar).first
    return if user.nil? || partner_profile.nil?

    Transaction.create_by_manual(@order)

    # Create app notification
    noti = Notification.create(
      target_type: Constants::NotificationType::ORDER,
      target: order.id,
      title: I18n.t('resources.order.accepted', partner: partner_profile.name),
      thumbnail: partner_profile.avatar,
      recipient: user.id,
      recipient_name: Constants::NotificationRecipientName::USER
    )
    user.add_unread_notification(noti)

    return unless user.device.notification_available?
    push_mobile_notification(
      user.device,
      order,
      Constants::EventTarget::USER,
      I18n.t('resources.order.accepted', partner: partner_profile.name)
    )
  end

  def request_partner_handler
    candidate = order.find_new_partner
    return if candidate.nil?
    device = candidate.device
    return if device.nil? || device.notification_available? == false

    # Take care of request partner in rake
    # edela-request-partner
    push_mobile_notification(
      device, order, Constants::EventTarget::PARTNER, I18n.t('resources.order.requested')
    )
  end

  def canceled_order_handler
    target = target_cancel_notification(order)
    actor = actor_cancel_notification(order)
    return if target.nil? || actor.nil?
    target_name = target.is_a?(User) ? Constants::EventTarget::USER : Constants::EventTarget::PARTNER

    # Create app notification
    noti = Notification.create(
      target_type: Constants::NotificationType::ORDER,
      target: order.id,
      title: I18n.t('resources.order.canceled'),
      thumbnail: actor.avatar,
      recipient: target.id,
      recipient_name: target_name
    )
    target.add_unread_notification(noti)

    # Push mobile notificaiton
    return unless target.device.notification_available?
    push_mobile_notification(target.device, order, target_name, I18n.t('resources.order.canceled'))
  end

  def process_order_handler
    user = User.joins(:device).where(id: order.user_id).includes(:device).first
    partner_profile = Partner::Profile.where(partner_id: order.partner_id).select(:name, :avatar).first
    return if user.nil? || partner_profile.nil?

    # Create app notification
    noti = Notification.create(
      target_type: Constants::NotificationType::ORDER,
      target: order.id,
      title: I18n.t('resources.order.process', partner: partner_profile.name),
      thumbnail: partner_profile.avatar,
      recipient: user.id,
      recipient_name: Constants::NotificationRecipientName::USER
    )
    user.add_unread_notification(noti)

    return unless user.device.notification_available?
    push_mobile_notification(
      user.device,
      order,
      Constants::EventTarget::USER,
      I18n.t('resources.order.process', partner: partner_profile.name)
    )
  end

  def finished_order_handler
    user = User.joins(:device).where(id: order.user_id).includes(:device).first
    partner_profile = Partner::Profile.where(partner_id: order.partner_id).select(:name, :avatar).first
    return if user.nil? || partner_profile.nil?

    # Create app notification
    noti = Notification.create(
      target_type: Constants::NotificationType::ORDER,
      target: order.id,
      title: I18n.t('resources.order.finished', partner: partner_profile.name),
      thumbnail: partner_profile.avatar,
      recipient: user.id,
      recipient_name: Constants::NotificationRecipientName::USER
    )
    user.add_unread_notification(noti)

    return unless user.device.notification_available?
    push_mobile_notification(
      user.device,
      order,
      Constants::EventTarget::USER,
      I18n.t('resources.order.finished', partner: partner_profile.name)
    )
  end

  def no_partner_for_order_handler
    # Send message to notification service to verify mobile number
    noti = NotificationStructure.new
    noti.title = I18n.t('resources.order.no_partner', order_id: order.id)
    noti.subscription.protocol = Constants::NotificationProtocol::SLACK_APP
    noti.event.name = event_name

    Shoryuken::Client.queues(ENV['NOTIFICATION_WORKER_QUEUE']).send_message(message_body: noti.to_json)
  end

  def device_valid?(device)
    device && device.device_token
  end

  def target_cancel_notification(order)
    target =
      if [Constants::OrderLevel::E10A, Constants::OrderLevel::E20A].include?(order.level)
        Partner.joins(:device).where(id: order.partner_id).includes(:device).first
      elsif [Constants::OrderLevel::E10B, Constants::OrderLevel::E20B].include?(order.level)
        User.joins(:device).where(id: order.user_id).includes(:device).first
      end

    target
  end

  def actor_cancel_notification(order)
    actor =
      if [Constants::OrderLevel::E10A, Constants::OrderLevel::E20A].include?(order.level)
        User::Profile.where(user_id: order.user_id).first
      elsif [Constants::OrderLevel::E10B, Constants::OrderLevel::E20B].include?(order.level)
        Partner::Profile.where(partner_id: order.partner_id).first
      end

    actor
  end

  def load_order
    Order.joins(:user_profile).where(id: event_data['order_id']).includes(:user_profile).first
  end

  def push_mobile_notification(device, order, target, title)
    # Send message to notification service to verify mobile number
    noti = NotificationStructure.new
    noti.title = title
    noti.subscription.endpoint = device.device_token
    noti.subscription.protocol = Constants::NotificationProtocol::PLATFORM
    noti.subscription.platform = device.os
    noti.event.name = event_name
    noti.event.data = { order: Order::NotificationSerializer.new(order) }
    noti.event.target = target

    Shoryuken::Client.queues(ENV['NOTIFICATION_WORKER_QUEUE']).send_message(message_body: noti.to_json)
  end
end
