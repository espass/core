class LogUserDeviceWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, backtrace: true
  sidekiq_options retry: 1

  def perform(user_id, dios)
    User::Device.find_or_create_by(user_id: user_id, device_id: dios)
  end
end
