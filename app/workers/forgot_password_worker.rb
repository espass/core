class ForgotPasswordWorker
  attr_reader :event_data, :event_name
  include Shoryuken::Worker

  shoryuken_options queue: ENV['PUSH_NOTIFICATION_WORKER_QUEUE'], auto_delete: true

  def perform(_sqs_msg, message)
    # Message schema: { event: 'event', data: { some_thing: '' } }
    event = JSON.parse(message)
    @event_name = event['event']['name']
    @event_data = event['event']['data']
    return unless event_name == Constants::NotificationEvent::FORGOT_PASSWORD

    send_email_forgot_password
  end

  private

  def send_email_forgot_password
    user = User.where(id: event_data['user_id']).includes(:profile).first
    return if user.nil?
    # Send email to user
    noti = NotificationStructure.new
    noti.content = ActionController::Base.new.render_to_string(
      template: 'email_templates/forgot_password.html',
      locals: {
        user_id: user.id,
        user_name: user.profile.name,
        reset_password_token: user.profile.reset_password_token
      }
    )
    noti.title = I18n.t('resources.user.forgot_password')
    noti.subscription.endpoint = user.email
    noti.subscription.protocol = Constants::NotificationProtocol::EMAIL
    noti.event.name = event_name
    noti.event.target = Constants::EventTarget::USER

    Shoryuken::Client.queues('edela-base-notification').send_message(message_body: noti.to_json)
  end
end
