class HandleNotificationWorker
  attr_reader :event_data, :event_name
  include Shoryuken::Worker

  shoryuken_options queue: ENV['PUSH_NOTIFICATION_WORKER_QUEUE'], auto_delete: true

  def perform(_sqs_msg, message)
    # Message schema: { event: 'event', data: { some_thing: '' } }
    event = JSON.parse(message)
    @event_name = event['event']['name']
    @event_data = event['event']['data']

    case event_name
    when Constants::NotificationEvent::VERIFY_MOBILE
      verify_mobile_handler
    end
  end

  private

  def verify_mobile_handler
    # Send message to notification service to verify mobile number
    noti = NotificationStructure.new
    noti.content = I18n.t('resources.user.your_pin', pin: event_data['pin'])
    noti.subscription.endpoint = event_data['mobile']
    noti.subscription.protocol = Constants::NotificationProtocol::SMS
    noti.event.name = event_name
    noti.event.target = Constants::EventTarget::USER

    Shoryuken::Client.queues('edela-verify').send_message(message_body: noti.to_json)
  end
end
