class BaseService < ApplicationRecord
  self.table_name = 'services'

  class << self
    def base_columns
      %w[id name description thumbnail]
    end
  end
end
