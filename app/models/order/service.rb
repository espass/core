class Order
  class Service < ApplicationRecord
    self.table_name = 'order_services'

    belongs_to :order, class_name: 'Order'
    belongs_to :partner_service, class_name: 'Partner::Service'

    validates :order_id, :partner_service_id, presence: true
  end
end
