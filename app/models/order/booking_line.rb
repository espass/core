# To manage partner searching for order
class Order
  class BookingLine < ApplicationRecord
    after_initialize :init

    has_many(
      :request_histories,
      class_name: 'Order::RequestHistory', foreign_key: 'order_id', primary_key: 'order_id'
    )
    belongs_to :order, class_name: 'Order', required: false
    belongs_to :partner, class_name: 'Partner', required: false, foreign_key: 'assigned_to'
    belongs_to :request, class_name: 'Order::RequestHistory', required: false, foreign_key: 'request_id'
    before_save :handle_state

    def handle_state
      return unless changes.key?('num_partner')

      case changes['num_partner'][1]
      when 1
        start(false)
      when 5
        finish(false)
      end
    end

    def request_new_partner(candidate)
      new_request = Order::RequestHistory.init_by_manual(candidate.id, order_id)
      self.status = Constants::BookingLineStatus::PROCESSING
      self.request = new_request
      self.assigned_to = candidate.id
      self.expiry_at = Time.now + 2.minutes
      self.num_partner += 1
      save!
    end

    def start(auto_save = true)
      self.status = Constants::BookingLineStatus::PROCESSING
      self.started_at = Time.now
      save if auto_save
    end

    def finish(auto_save = true)
      self.status = Constants::BookingLineStatus::FINISHED
      self.finished_at = Time.now
      save if auto_save
    end

    def finished?
      status == Constants::BookingLineStatus::FINISHED
    end

    def acceptable?(partner)
      return false unless assigned_to == partner.id
      return false if finished?
      return false if request.nil?
      true
    end

    def declinable?(partner)
      return false unless assigned_to == partner.id
      return false if finished?
      return false if request.nil?
      true
    end

    def accept
      finish && request && request.accept
    end

    def decline(note)
      request.decline(note)
    end

    private

    def init
      self.num_partner ||= 0
      self.status ||= Constants::BookingLineStatus::PENDING
    end
  end
end
