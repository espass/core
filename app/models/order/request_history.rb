class Order
  class RequestHistory < ApplicationRecord
    after_initialize :init

    belongs_to :partner, class_name: 'Partner', required: false
    belongs_to :order, class_name: 'Order', required: false
    belongs_to(
      :booking_line,
      class_name: 'Order::BookingLine', foreign_key: 'order_id', primary_key: 'order_id',
      required: false
    )

    def init
      self.status ||= Constants::BookingRequestStatus::PENDING
    end

    def accept
      self.status = Constants::BookingRequestStatus::ACCEPTED
      save
    end

    def decline(reason = '')
      self.status = Constants::BookingRequestStatus::DECLINED
      self.note = reason
      save
    end

    class << self
      def init_by_manual(partner_id, order_id)
        new(partner_id: partner_id, order_id: order_id)
      end
    end
  end
end
