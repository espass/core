class Order < ApplicationRecord
  self.table_name = 'orders'
  after_initialize :init

  serialize :attachments, Array
  serialize :base_services, Array

  belongs_to :user, class_name: 'User', required: false
  belongs_to :partner, class_name: 'Partner', required: false
  has_many :services, class_name: 'Order::Service'
  has_many :reviews, -> { where(is_approved: true, is_deleted: false) }, class_name: 'Partner::Review'

  has_one :user_profile, class_name: 'User::Profile', foreign_key: 'user_id', primary_key: 'user_id'
  has_one :partner_profile, class_name: 'Partner::Profile', foreign_key: 'partner_id', primary_key: 'partner_id'
  has_one :booking_line, class_name: 'Order::BookingLine'

  validates :booking_time, :location, :address, presence: true
  validates_inclusion_of :status, in: Constants.OrderStatusValues
  validates_inclusion_of :level, in: Constants.OrderLevelValues

  before_create :handle_before_create

  def rollback
    services.destroy_all
    destroy
  end

  def requested?
    status == Constants::OrderStatus::REQUESTED && partner_id.nil?
  end

  def success?
    status == Constants::OrderStatus::SUCCESS
  end

  def pending?
    status == Constants::OrderStatus::PENDING
  end

  def processing?
    status == Constants::OrderStatus::PROCESSING
  end

  def acceptable?(partner)
    # Required
    # self valid
    # Booking line valid
    # Partner can accept order
    requested? && booking_line.present? && booking_line.acceptable?(partner) && partner.order_acceptable?(self)
  end

  def declinable?(partner)
    requested? && booking_line.present? && booking_line.declinable?(partner)
  end

  def cancelable?(actor)
    (pending? || requested?) && actor.order_cancelable?(self)
  end

  def processable?(partner)
    pending? && partner.order_processable?(self)
  end

  def finishable?(actor)
    processing? && actor.order_finishable?(self)
  end

  def partner_accepted(booking_line)
    self.status = Constants::OrderStatus::PENDING
    self.level = Constants::OrderLevel::E21
    self.partner_id = booking_line.assigned_to

    return false unless save
    # Close booking line & update request status
    booking_line.accept
    # Push notification
    push_order_notification(Constants::NotificationEvent::ACCEPTED_ORDER)
    # Update elasticsearch index
    UpdateOrderIndexWorker.perform_async(id)
    true
  end

  def partner_decline(params)
    # Sync
    # Cancel old request
    booking_line.decline(params[:note])
    request_partner
  end

  def cancel(actor)
    self.status = Constants::OrderStatus::CANCELED
    self.level =
      if actor.is_a?(User)
        requested? ? Constants::OrderLevel::E10A : Constants::OrderLevel::E20A
      else
        requested? ? Constants::OrderLevel::E10B : Constants::OrderLevel::E20B
      end
    return false unless save
    # Handle notification for partner
    push_order_notification(Constants::NotificationEvent::CANCELED_ORDER)
    # Update elasticsearch index
    UpdateOrderIndexWorker.perform_async(id)
    true
  end

  def process
    self.status = Constants::OrderStatus::PROCESSING
    self.level = Constants::OrderLevel::E31
    return false unless save
    # Handle notification for partner
    push_order_notification(Constants::NotificationEvent::PROCESS_ORDER)
    # Update elasticsearch index
    UpdateOrderIndexWorker.perform_async(id)
    true
  end

  def finish(params)
    self.status = Constants::OrderStatus::SUCCESS
    self.level = Constants::OrderLevel::E41
    self.finished_at = Time.now
    self.price = params[:price] if params[:price].present?
    return false unless save
    # + Update transaction
    # + Push notification
    push_order_notification(Constants::NotificationEvent::FINISHED_ORDER)
    # Update elasticsearch index
    UpdateOrderIndexWorker.perform_async(id)
    true
  end

  def request_partner
    return if Rails.env.test?
    # Push message to find the new partner
    # To be continued
    push_order_notification(Constants::NotificationEvent::REQUEST_PARTNER)
  end

  def find_new_partner
    # Find the new partner
    # Booking Line
    # Update new request
    # Update assigned to
    # Update expiry_at
    booking_line = self.booking_line
    requested_partner_ids = []

    if booking_line.nil?
      booking_line = Order::BookingLine.new(order_id: id)
    else
      requested_partner_ids = booking_line.request_histories.pluck(:partner_id)
    end

    candidates = PartnerServices.quick_looking(self)
    candidates -= requested_partner_ids # Remove requested from candidates
    candidate = Partner.where(id: candidates.first).first if candidates.present?

    # No one to request
    # Warning....
    if candidate.nil?
      booking_line.finish
      push_order_notification(Constants::NotificationEvent::NO_PARTNER)
      return
    end
    booking_line.request_new_partner(candidate)

    candidate
  end

  def valid_for_me(partner)
    self.level = Constants::OrderLevel::E11
    self.status = Constants::OrderStatus::REQUESTED
    self.partner_id = nil
    booking_line.status = Constants::BookingLineStatus::PENDING
    booking_line.assigned_to = partner.id
    booking_line.save
    save
  end

  def owner?(resource)
    resource.is_a?(Partner) ? partner_id == resource.id : user_id == resource.id
  end

  private

  def init
    self.level ||= Constants::OrderLevel::E11 if has_attribute?(:level)
    self.status ||= Constants::OrderStatus::PENDING if has_attribute?(:status)
    self.booking_type ||= Constants::BookingType::NORMAL if has_attribute?(:booking_type)
  end

  def handle_before_create
    # Validate order
    validate_exist
  end

  def validate_exist
    pending_condition = {
      user_id: user_id,
      partner_id: partner_id,
      status: Constants::OrderStatus::PENDING
    }
    return if Order.where(pending_condition).count.zero?

    errors.add(:order, I18n.t('activerecord.errors.models.order.messages.duplicated'))
    throw :abort
  end

  def push_order_notification(event_name)
    return if Rails.env.test?
    noti_data = {}
    noti_data[:event] = { name: event_name, data: { order_id: id } }
    OrderWorker.perform_async(noti_data.to_json)
  end

  class << self
    def booking_columns
      %w[id location address price status]
    end

    def quick_booking_columns
      %w[id booking_type attachments base_services location address status]
    end

    def notification_columns
      %w[id location booking_time address price status attachments base_services]
    end

    def base_columns
      %w[id location booking_time address price status base_services attachments]
    end

    def report_columns
      %w[price finished_at]
    end
  end
end
