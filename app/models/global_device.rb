class GlobalDevice < ApplicationRecord
  self.table_name = 'devices'
  validates_inclusion_of :os, in: Constants.DeviceValues

  def update_info(params)
    %w[os os_version lang device_token device_id].each do |fie|
      vlue = params[fie]
      self[fie] = vlue unless vlue.blank?
    end
    save
  end

  def notification_available?
    device_token.present?
  end
end
