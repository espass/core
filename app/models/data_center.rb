class DataCenter
  class << self
    def current_user=(user)
      Thread.current[:current_user] = user
    end

    def current_user
      Thread.current[:current_user]
    end

    def current_partner=(partner)
      Thread.current[:current_partner] = partner
    end

    def current_partner
      Thread.current[:current_partner]
    end

    def current_resource=(resource)
      Thread.current[:current_resource] = resource
    end

    def current_resource
      Thread.current[:current_resource]
    end
  end
end
