class Transaction < ApplicationRecord
  self.table_name = 'transactions'
  after_initialize :init

  belongs_to :user, class_name: 'User'
  belongs_to :partner, class_name: 'Partner'
  belongs_to :order, class_name: 'Order'

  validates :status, :amount, presence: true
  validates_inclusion_of :method, in: Constants.TransactionMethodValues
  validates_inclusion_of :status, in: Constants.TransactionStatusValues

  private

  def init
    self.method ||= Constants::TransactionMethod::CASH
    self.amount ||= 0
    self.status ||= Constants::TransactionStatus::PENDING
  end

  class << self
    def create_by_manual(order)
      find_or_create_by(order_id: order.id)
    end
  end
end
