class User
  class Favorite < ApplicationRecord
    self.table_name = 'user_favorites'

    belongs_to :user, class_name: 'User', required: false
    belongs_to :partner, class_name: 'Partner'
  end
end
