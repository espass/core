class User
  class Profile < ApplicationRecord
    self.table_name = 'user_profiles'
    serialize :unread_notifications, Array
    belongs_to :user, class_name: 'User'
    after_initialize :init

    def valid_pin?(pin_code)
      if pin_code.to_i == pin && pin_sent_at > Time.now - 5.minutes
        self.is_verified = true
        save
        return true
      end

      false
    end

    def verify_mobile(mobile_number)
      if mobile == mobile_number && is_verified
        errors[:mobile_verified] << I18n.t('resources.user.verified')
        return
      end

      unverify
      generate_pin(mobile_number)

      return self unless save

      # Push notification
      noti_data = {}
      noti_data[:event] = {
        name: Constants::NotificationEvent::VERIFY_MOBILE,
        data: { pin: pin, mobile: mobile, source_id: id }
      }
      HandleNotificationWorker.perform_async(noti_data.to_json) unless Rails.env.test?

      true
    end

    private

    def init
      self.is_verified = false if has_attribute?(:is_verified) && is_verified.nil?
      self.num_unread_notification ||= 0 if has_attribute?(:num_unread_notification)
    end

    def unverify
      self.is_verified = false
      self.pin_sent_at = nil
      self.pin = nil
    end

    def generate_pin(mobile_number)
      self.mobile = mobile_number
      self.pin = rand(10000..99999)
      self.pin_sent_at = Time.now
    end

    class << self
      def auth_columns
        %w[name mobile cover_photo is_verified user_id num_unread_notification]
      end

      def preview_columns
        %w[name avatar]
      end

      def review_columns
        %w[user_id avatar name]
      end

      def base_columns
        %w[
          name mobile address location
          description avatar cover_photo
          is_verified num_unread_notification
        ]
      end
    end
  end
end
