class User
  class Device < ApplicationRecord
    self.table_name = 'user_devices'

    belongs_to :user, class_name: 'User', required: false
    belongs_to :device, class_name: 'GlobalDevice', required: false
  end
end
