class Issue < ApplicationRecord
  self.table_name = 'issues'
  after_initialize :init

  belongs_to :user, class_name: 'User', required: false
  belongs_to :partner, class_name: 'Partner', required: false

  validates :description, presence: true
  validates_inclusion_of :issue_type, in: Constants.IssueTypeValues

  private

  def init
    self.issue_type ||= Constants::IssueType::OTHER
  end

  class << self
    def base_columns
      %w[created_at title description issue_type]
    end

    def create_by_manual(params, actor)
      issue = Issue.new
      %w[title description issue_type].each do |field|
        value = params[field]
        issue[field] = value if value.present?
      end
      actor.is_a?(User) ? issue.user_id = actor.id : issue.partner_id = actor.id
      issue
    end
  end
end
