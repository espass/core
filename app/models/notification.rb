class Notification < ApplicationRecord
  self.table_name = 'notifications'
  after_initialize :init

  validates_inclusion_of :recipient_name, in: Constants.NotificationRecipientNameValues
  validates_inclusion_of :target_type, in: Constants.NotificationTypeValues

  def mark_delete(author)
    unless recipient == author.id && recipient_name == author.class.to_s.downcase
      errors.add(:user_id, I18n.t('errors.messages.no_permission'))
      return false
    end

    self.is_deleted = true
    save
  end

  def description
    created_at.strftime('%d/%m/%Y %H:%M')
  end

  private

  def init
    self.is_deleted ||= false
    self.target_type ||= Constants::NotificationType::SYSTEM
  end

  class << self
    def base_fields
      %w[id target_type target title description thumbnail]
    end
  end
end
