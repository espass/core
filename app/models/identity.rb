class Identity < ApplicationRecord
  self.table_name = 'identities'

  belongs_to :user, class_name: 'User', required: false

  def self.find_for_oauth(auth)
    find_or_initialize_by(uid: auth['uid'], provider: auth['provider'])
  end
end
