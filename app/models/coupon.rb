class Coupon < ApplicationRecord
  self.table_name = 'coupons'
  after_initialize :init

  validates_presence_of :code, :expiry_at, :discount, :discount_type
  validates_inclusion_of :discount_type, in: Constants.DiscountTypeValues

  def expired?
    expiry_at < Time.now
  end

  private

  def init
    self.discount_type ||= Constants::DiscountType::FIXED_BOOKING if has_attribute?(:discount_type)
    self.usage_limit_per_user ||= 1 if has_attribute?(:usage_limit_per_user)
  end
end
