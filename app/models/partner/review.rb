class Partner
  class Review < ApplicationRecord
    self.table_name = 'partner_reviews'
    after_initialize :init

    belongs_to :partner, class_name: 'Partner', required: false

    belongs_to :user, class_name: 'User', required: false
    has_one(
      :user_profile,
      -> { select(User::Profile.review_columns) },
      class_name: 'User::Profile', foreign_key: 'user_id', primary_key: 'user_id'
    )

    belongs_to :order, class_name: 'Order', required: false

    validates :rating, :partner_id, :user_id, presence: true

    private

    def init
      self.is_approved ||= true if has_attribute?(:is_approved)
      self.is_deleted ||= false if has_attribute?(:is_deleted)
    end

    class << self
      def base_columns
        %w[created_at title description rating user_id]
      end

      def create_by_manual(params)
        review = find_or_initialize_by(order_id: params[:order_id])
        %w[title description rating].each do |field|
          review[field] = params[field]
        end

        review
      end
    end
  end
end
