class Partner
  class RatingReport < ApplicationRecord
    self.table_name = 'partner_rating_reports'
    after_initialize :init

    belongs_to :partner, class_name: 'Partner', required: false

    private

    def init
      self.one ||= 0 if has_attribute?(:one)
      self.two ||= 0 if has_attribute?(:two)
      self.three ||= 0 if has_attribute?(:three)
      self.four ||= 0 if has_attribute?(:four)
      self.five ||= 0 if has_attribute?(:five)
      self.num_rate ||= 0 if has_attribute?(:num_rate)
      self.rating ||= 0.0 if has_attribute?(:rating)
    end

    class << self
      def base_columns
        %w[num_rate rating one two three four five]
      end
    end
  end
end
