class Partner
  class Photo < ApplicationRecord
    self.table_name = 'partner_photos'
    after_initialize :init

    belongs_to :partner, class_name: 'Partner', required: false
    belongs_to :album, class_name: 'Partner::Album', required: false

    validates :link, presence: true

    def mark_delete(partner)
      unless partner_id == partner.id
        errors.add(:user_id, I18n.t('errors.messages.no_permission'))
        return false
      end

      self.is_deleted = true
      save
    end

    private

    def init
      self.featured = false if has_attribute?(:featured) && featured.nil?
      self.is_deleted = false if has_attribute?(:is_deleted) && is_deleted.nil?
    end

    class << self
      def base_columns
        %w[id title link featured partner_id]
      end
    end
  end
end
