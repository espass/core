class Partner
  class WorkingTime < ApplicationRecord
    self.table_name = 'working_times'
    after_initialize :init

    serialize :repeat, Array
    belongs_to :partner, class_name: 'Partner', required: false
    validates :from, :to, :repeat, presence: true

    def update_by_manual(params)
      self.from = params[:from] if WorkingTime.duration_valid?(params[:from])
      self.to = params[:to] if WorkingTime.duration_valid?(params[:to])
      self.repeat = params[:repeat] if WorkingTime.repeat_valid?(params[:repeat])
      self.enabled = params[:enabled] unless params[:enabled].blank?
      save
    end

    private

    def init
      self.enabled = true if enabled.nil?
    end

    class << self
      def base_columns
        %w[from to repeat enabled]
      end

      def duration_valid?(duration)
        return false if duration.blank?
        duration = duration.to_s unless duration.is_a?(String)
        return false if duration.match(/^[0-9]+$/).nil?
        true
      end

      def repeat_valid?(repeat)
        return false if repeat.blank?
        return false unless repeat.is_a?(Array)
        return false if repeat.join('').match(/^[0-6]+$/).nil?
        true
      end
    end
  end
end
