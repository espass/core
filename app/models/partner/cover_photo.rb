class Partner
  class CoverPhoto < ApplicationRecord
    self.table_name = 'partner_cover_photos'
    after_initialize :init

    belongs_to :partner, class_name: 'Partner', required: false
    validates :link, presence: true

    private

    def init
      self.is_deleted = false if has_attribute?(:is_deleted) && is_deleted.nil?
    end

    class << self
      def base_columns
        %w[created_at title link]
      end

      def create_by_manual(params, partner)
        cover_photo = CoverPhoto.new
        cover_photo.partner = partner
        %w[title link].each do |field|
          value = params[field]
          cover_photo[field] = value if value.present?
        end
        cover_photo
      end
    end
  end
end
