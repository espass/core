class Partner
  class Profile < ApplicationRecord
    self.table_name = 'partner_profiles'
    serialize :unread_notifications, Array
    after_initialize :init

    belongs_to :partner, class_name: 'Partner'

    private

    def init
      return unless has_attribute?(:rating)
      self.rating ||= 0.0
      self.num_rate ||= 0
      self.min_price ||= 0
      self.max_price ||= 0
      self.num_unread_notification ||= 0 if has_attribute?(:num_unread_notification)
      self.num_album ||= 0 if has_attribute?(:num_album) && num_album.nil?
      self.year_exp ||= 0 if has_attribute?(:year_exp) && year_exp.nil?
    end

    class << self
      def auth_columns
        %w[name mobile avatar partner_id num_unread_notification]
      end

      def preview_columns
        %w[name avatar address rating num_rate min_price max_price partner_id]
      end

      def base_columns
        %w[
          name avatar address location rating
          num_rate num_album min_price max_price
          description year_exp num_unread_notification
        ]
      end
    end
  end
end
