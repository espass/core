class Partner
  class Album < ApplicationRecord
    self.table_name = 'partner_albums'
    after_initialize :init

    has_many :photos, -> { where(is_deleted: false) }, class_name: 'Partner::Photo', foreign_key: 'album_id'
    belongs_to :partner, class_name: 'Partner', required: false

    validates :title, presence: true

    def update_num_photo
      self.num_photo = photos.count
      save
    end

    def add_photos(partner, photos_data)
      unless partner_id == partner.id
        errors.add(:user_id, I18n.t('errors.messages.no_permission'))
        return false
      end

      photos_data = photos_data.as_json(only: %w[title link])
      photos_data.each do |pt|
        photo = photos.new(pt)
        photo.partner_id = partner_id
        unless photo.save
          errors.add(:order, I18n.t('resources.partner.album.could_not_add_photo'))
          return false
        end

        self.thumbnail = photo.link
      end
      save
    end

    def mark_delete(partner)
      unless partner_id == partner.id
        errors.add(:user_id, I18n.t('errors.messages.no_permission'))
        return false
      end
      # Maybe move into queue in the near future
      photos.update_all(is_deleted: true)

      self.is_deleted = true
      save
    end

    def delete_photos(partner, photo_ids)
      unless partner_id == partner.id
        errors.add(:user_id, I18n.t('errors.messages.no_permission'))
        return false
      end

      result = photos.where(id: photo_ids).update_all(is_deleted: true)
      self.num_photo -= result
      save
    end

    private

    def init
      self.is_deleted = false if has_attribute?(:is_deleted) && is_deleted.nil?
      self.num_photo ||= 0
    end

    class << self
      def base_columns
        %w[id partner_id num_photo title thumbnail]
      end

      def create_by_manual(params)
        album = new
        %w[title description].each do |field|
          album[field] = params[field]
        end

        album
      end
    end
  end
end
