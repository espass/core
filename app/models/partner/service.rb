class Partner
  class Service < ApplicationRecord
    self.table_name = 'partner_services'

    belongs_to :partner, class_name: 'Partner', required: false
    belongs_to :service, class_name: 'BaseService', required: false

    class << self
      def base_columns
        %w[service_id name]
      end
    end
  end
end
