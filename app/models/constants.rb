module Constants
  module AuthProvider
    FB = 'facebook'.freeze
  end

  module OrderStatus
    REQUESTED = 'requested'.freeze
    PENDING = 'pending'.freeze
    PROCESSING = 'processing'.freeze
    SUCCESS = 'success'.freeze
    CANCELED = 'canceled'.freeze
  end

  module OrderLevel
    E10A = 'E1.0.A'.freeze
    E10B = 'E1.0.B'.freeze
    E11 = 'E1.1'.freeze
    E12 = 'E1.2'.freeze
    E20A = 'E2.0.A'.freeze
    E20B = 'E2.0.B'.freeze
    E21 = 'E2.1'.freeze
    E30A = 'E3.0.A'.freeze
    E30B = 'E3.0.B'.freeze
    E31 = 'E3.1'.freeze
    E40A = 'E4.0.A'.freeze
    E40B = 'E4.0.B'.freeze
    E41 = 'E4.1'.freeze
  end

  module BookingType
    NORMAL = 'normal'.freeze
    QUICKLY = 'quickly'.freeze
  end

  module MediaType
    IMAGE = 'image'.freeze
  end

  module NotificationEvent
    NEW_ORDER = 'new_order'.freeze
    ACCEPTED_ORDER = 'accepted_order'.freeze
    VERIFY_MOBILE = 'verify_mobile'.freeze
    REQUEST_PARTNER = 'request_partner'.freeze
    CANCELED_ORDER = 'canceled_order'.freeze
    FINISHED_ORDER = 'finished_order'.freeze
    PROCESS_ORDER = 'process_order'.freeze
    FORGOT_PASSWORD = 'forgot_password'.freeze
    NO_PARTNER = 'no_partner'.freeze
  end

  module NotificationProtocol
    SMS = 'sms'.freeze
    EMAIL = 'email'.freeze
    PLATFORM = 'platform'.freeze
    SLACK_APP = 'slack_app'.freeze
  end

  module NotificationType
    ORDER = 'order'.freeze
    SYSTEM = 'system'.freeze
  end

  module NotificationRecipientName
    PARTNER = 'partner'.freeze
    USER = 'user'.freeze
    GENERAL = 'general'.freeze
  end

  module Device
    IOS = 'ios'.freeze
    ANDROID = 'android'.freeze
  end

  module BookingLineStatus
    PENDING = 'pending'.freeze
    PROCESSING = 'processing'.freeze
    FINISHED = 'finished'.freeze
  end

  module BookingRequestStatus
    PENDING = 'pending'.freeze
    ACCEPTED = 'accepted'.freeze
    DECLINED = 'declined'.freeze
  end

  module EventTarget
    USER = 'user'.freeze
    PARTNER = 'partner'.freeze
  end

  module TransactionMethod
    CASH = 'cash'.freeze
  end

  module TransactionStatus
    PENDING = 'pending'.freeze
    CANCELED = 'canceled'.freeze
    SUCCESS = 'success'.freeze
  end

  module IssueType
    OTHER = 'other'.freeze
  end

  module AppName
    USER = 'user'.freeze
    PARTNER = 'partner'.freeze
  end

  module DiscountType
    FIXED_BOOKING = 'fixed_booking'.freeze
    FIXED_SERVICE = 'fixed_service'.freeze
  end

  class << self
    Constants.constants.each do |module_name|
      sub_module = Constants.const_get(module_name)
      next unless sub_module.is_a?(Module)

      define_method(module_name.to_s + 'Values') do
        sub_module.constants.collect { |const_name| sub_module.const_get(const_name) }
      end
    end
  end
end
