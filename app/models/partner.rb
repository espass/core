class Partner < ApplicationRecord
  self.table_name = 'partners'

  has_secure_password

  belongs_to :device, class_name: 'GlobalDevice', required: false
  has_one :profile, class_name: 'Partner::Profile'
  has_one :rating_report, class_name: 'Partner::RatingReport'
  has_many :services, class_name: 'Partner::Service'
  has_many :orders, class_name: 'Order'
  has_many :transactions, class_name: 'Transaction'
  has_many :working_times, class_name: 'Partner::WorkingTime'
  # Load working today time
  has_many :working_today_time, lambda {
    where('enabled = ? AND working_times.repeat LIKE ?', true, "%#{Time.now.wday}%")
  }, class_name: 'Partner::WorkingTime'

  has_many :albums, -> { where(is_deleted: false) }, class_name: 'Partner::Album'
  has_many :featured_images, lambda {
    where(is_deleted: false).select(Partner::CoverPhoto.base_columns).order(id: :desc).limit(3)
  }, class_name: 'Partner::CoverPhoto'
  has_many :reviews, -> { where(is_approved: true, is_deleted: false) }, class_name: 'Partner::Review'

  # Association with special columns
  has_one :profile_only_fetch_auth, -> { select(Profile.auth_columns) }, class_name: 'Partner::Profile'
  has_one :profile_only_fetch_preview, -> { select(Profile.preview_columns) }, class_name: 'Partner::Profile'

  validates :email, presence: true
  validates :email, uniqueness: true

  def generate_new_secure_token
    self.auth_token = 'P' + BCrypt::Password.create(SecureRandom.urlsafe_base64)
    self.token_expiry = Time.now + 1.days
  end

  def update_extra_params(extra_params)
    %i[last_sign_in_at last_sign_in_ip device_id].each do |fie|
      vlue = extra_params[fie]
      self[fie] = vlue unless vlue.blank?
    end
  end

  def refresh_token(extra_params, auto_save = false)
    generate_new_secure_token
    update_extra_params(extra_params)

    save if auto_save
  end

  def destroy_token
    self.token_expiry = Time.now.yesterday
    save
  end

  def my_orders(condition = {})
    condition = {} unless condition.is_a?(Hash)
    Order.where(condition.merge(partner_id: id))
  end

  def coming_soon_orders
    orders = Order.where(
      'partner_id = ? AND status IN (?) AND booking_time < ?',
      id,
      [Constants::OrderStatus::PROCESSING, Constants::OrderStatus::PENDING],
      Time.now + 7.days
    ).order(:booking_time)

    orders
  end

  def my_order_history
    Order.where(
      'partner_id = ? AND status IN (?)',
      id,
      [Constants::OrderStatus::CANCELED, Constants::OrderStatus::SUCCESS]
    ).order(id: :desc).includes(:user_profile)
  end

  def scheduled_orders
    orders = Order.where(
      'partner_id = ? AND status IN (?)',
      id,
      [Constants::OrderStatus::PROCESSING, Constants::OrderStatus::PENDING]
    ).order(booking_time: :desc).includes(:user_profile)

    orders
  end

  def my_notifications
    Notification.where(
      recipient_name: Constants::NotificationRecipientName::PARTNER,
      recipient: id
    ).or(
      Notification.where(recipient_name: Constants::NotificationRecipientName::GENERAL)
    ).where(is_deleted: false)
  end

  # Order acceptable defined:
  # This partner has no order has the same time with the user' request time
  # The order assigned to this partner?
  def order_acceptable?(order)
    return false unless order.is_a?(Order)

    period_time = order.booking_time.ago(1.hours)..(order.booking_time + 1.hours)
    valid_period = my_orders.where(
      booking_time: period_time,
      status: Constants::OrderStatus::PENDING
    ).count.zero?

    valid_period
  end

  def order_cancelable?(order)
    order.partner_id == id
  end

  def order_processable?(order)
    order.partner_id == id
  end

  def order_finishable?(order)
    order.partner_id == id
  end

  def load_rating_report
    RatingReport.find_or_create_by(partner_id: id)
  end

  def find_or_init_profile
    profile = self.profile
    if profile.nil?
      profile = Partner::Profile.new
      profile.partner = self
    end
    self.profile = profile

    profile
  end

  def update_profile(params)
    profile = find_or_init_profile

    %w[name address location avatar description mobile min_price max_price year_exp].each do |fie|
      vlue = params[fie]
      profile[fie] = vlue unless vlue.blank?
    end

    unless profile.save
      errors.add(:profile, ApplicationHelper.pretty_error(profile))
      return false
    end

    true
  end

  def update_services(params)
    old_services = services.to_a
    # Remove services
    old_services.each do |service|
      current_service = params[:services].detect do |sv|
        sv['service_id'].to_i == service['service_id']
      end
      service.delete if current_service.nil?
    end

    # Update or create new service
    params[:services].each do |service|
      current_service = old_services.detect { |sv| sv['service_id'] == service['service_id'].to_i }
      current_service = Service.new(partner_id: id) if current_service.nil?
      %w[name description price thumbnail service_id].each do |field|
        current_service[field] = service[field]
      end

      unless current_service.save
        errors.add(:services, ApplicationHelper.pretty_error(current_service))
        return false
      end
    end
  end

  def add_unread_notification(notification)
    return false if profile.nil?
    # Update user's unread information
    profile.unread_notifications << notification.id
    profile.num_unread_notification += 1
    profile.save
  end

  def unread_notification?(notification_id)
    return false if profile.nil?
    profile.unread_notifications.include?(notification_id)
  end

  # Mark notification as read
  def read_notification(notification)
    return true if profile.nil?
    return true unless profile.partner_id == notification.recipient
    return true unless profile.unread_notifications.include?(notification.id)
    profile.unread_notifications.delete(notification.id)
    profile.num_unread_notification -= 1 if profile.num_unread_notification > 0
    profile.save
  end
end
