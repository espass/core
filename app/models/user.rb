class User < ApplicationRecord
  self.table_name = 'users'
  has_secure_password

  belongs_to :device, class_name: 'GlobalDevice', required: false
  has_one :profile, class_name: 'User::Profile'
  has_many :orders, class_name: 'Order'
  has_many :favorites, class_name: 'User::Favorite'
  has_many :transactions, class_name: 'Transaction'
  has_many :devices, class_name: 'User::Device'

  # Association with special columns
  has_one :profile_only_fetch_auth, -> { select(Profile.auth_columns) }, class_name: 'User::Profile'

  validates :email, presence: true
  validates :email, uniqueness: true
  validates_presence_of :password, on: :create

  def generate_new_secure_token
    self.auth_token = 'U' + BCrypt::Password.create(SecureRandom.urlsafe_base64)
    self.token_expiry = Time.now + 1.days
  end

  def generate_reset_password_token
    profile = find_or_init_profile
    profile.reset_password_token = 'U' + SecureRandom.hex
    profile.reset_password_expiry = Time.now + 1.days
    profile.save
  end

  def update_extra_params(extra_params)
    %i[last_sign_in_at last_sign_in_ip device_id].each do |fie|
      vlue = extra_params[fie]
      self[fie] = vlue unless vlue.blank?
    end
  end

  def refresh_token(extra_params, auto_save = false)
    generate_new_secure_token
    update_extra_params(extra_params)

    save if auto_save
  end

  def destroy_token
    self.token_expiry = Time.now.yesterday
    save
  end

  def find_or_init_profile
    profile = self.profile
    if profile.nil?
      profile = User::Profile.new
      profile.user = self
    end
    self.profile = profile

    profile
  end

  def update_profile(params)
    profile = find_or_init_profile

    %w[location mobile address name description avatar cover_photo].each do |fie|
      vlue = params[fie]
      profile[fie] = vlue unless vlue.blank?
    end

    profile.save
  end

  def my_orders(condition = {})
    condition = {} unless condition.is_a?(Hash)
    Order.where(condition.merge(user_id: id))
  end

  def my_order_history
    Order.where(
      'user_id = ? AND status IN (?)',
      id,
      [Constants::OrderStatus::CANCELED, Constants::OrderStatus::SUCCESS]
    ).order(id: :desc).includes(:partner_profile)
  end

  def scheduled_orders
    orders = Order.where(
      'user_id = ? AND status IN (?)',
      id,
      [Constants::OrderStatus::PROCESSING, Constants::OrderStatus::PENDING, Constants::OrderStatus::REQUESTED]
    ).order(booking_time: :desc).includes(:partner_profile)

    orders
  end

  def favorite_partner(partner_id)
    User::Favorite.find_or_initialize_by(user_id: id, partner_id: partner_id)
  end

  def my_partners
    partner_ids = User::Favorite.where(user_id: id).pluck(:partner_id)
    Partner.where(id: partner_ids).includes(:profile_only_fetch_preview)
  end

  def my_notifications
    Notification.where(
      recipient_name: Constants::NotificationRecipientName::USER,
      recipient: id
    ).or(
      Notification.where(recipient_name: Constants::NotificationRecipientName::GENERAL)
    ).where(is_deleted: false)
  end

  def favorited?(partner_id)
    favorites.pluck(:partner_id).include?(partner_id)
  end

  def unread_notification?(notification_id)
    return false if profile.nil?
    profile.unread_notifications.include?(notification_id)
  end

  def add_unread_notification(notification)
    return false if profile.nil?
    # Update user's unread information
    profile.unread_notifications << notification.id
    profile.num_unread_notification += 1
    profile.save
  end

  # Mark notification as read
  def read_notification(notification)
    return true if profile.nil?
    return true unless profile.user_id == notification.recipient
    return true unless profile.unread_notifications.include?(notification.id)
    profile.unread_notifications.delete(notification.id)
    profile.num_unread_notification -= 1 if profile.num_unread_notification > 0
    profile.save
  end

  def order_cancelable?(order)
    order.user_id == id
  end

  def order_finishable?(_order)
    true
  end

  def valid_reset_password_token?(token)
    token == profile.reset_password_token && profile.reset_password_expiry > Time.now
  end

  def disable_reset_password_token
    profile.reset_password_token = nil
    profile.reset_password_expiry = nil
    profile.save
  end

  def update_password(new_password)
    self.password = new_password
    save
  end

  def log_device(params)
    return if params[:dios].blank?
    LogUserDeviceWorker.perform_async(id, params[:dios])
  rescue StandardError
    nil
  end

  class << self
    def handle_for_oauth(oauth, extra_authen_params)
      # Get the identity and user
      identity = Identity.find_for_oauth(oauth)

      user = load_user_by_oauth(oauth) if user.nil?
      user.refresh_token(extra_authen_params)
      user.save!
      user.profile = load_profile_by_oauth(user, oauth)

      if identity.user_id != user.id
        identity.user_id = user.id
        identity.save
      end

      user
    end

    def load_profile_by_oauth(user, oauth)
      profile = user.profile || User::Profile.new
      profile.name = oauth['name']
      profile.avatar = load_avatar_from_oauth(oauth)
      profile.user = user
      profile.save!

      profile
    end

    def load_user_by_oauth(oauth)
      email = oauth['email'] || "#{oauth['id']}_#{oauth['provider']}@edela.vn"
      user = User.find_by_email(email)
      user = User.new(email: email, password: SecureRandom.urlsafe_base64) if user.nil?

      user
    end

    def load_avatar_from_oauth(oauth)
      case oauth['provider']
      when Constants::AuthProvider::FB
        'http://graph.facebook.com/100005328395088/picture?width=300'
      else
        ''
      end
    end
  end
end
