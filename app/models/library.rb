class Library < ApplicationRecord
  self.table_name = 'libraries'

  after_initialize :init

  belongs_to :user, class_name: 'User', required: false
  belongs_to :partner, class_name: 'Partner', required: false

  validates :key, :url, presence: true

  def self.create_by_manual(key, current_resource)
    lib = new(key: key)
    if current_resource.class == User
      lib.user_id = current_resource.id
    else
      lib.partner_id = current_resource.id
    end
    lib.save

    lib
  end

  def mark_delete(author)
    is_author = author.is_a?(User) ? user_id == author.id : partner_id == author.id

    unless is_author
      errors.add(:user_id, I18n.t('errors.messages.no_permission'))
      return false
    end

    self.is_deleted = true
    save
  end

  private

  def init
    self.media_type ||= Constants::MediaType::IMAGE
    self.url ||= build_library_url
    self.is_deleted ||= false
  end

  def build_library_url
    "#{ENV['AWS_CF_MEDIA']}/#{key}"
  end
end
