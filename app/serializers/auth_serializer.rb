class AuthSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attr_reader :id, :email, :auth_token, :object, :resource
  attributes :id, :email, :auth_token, :profile, :links

  def initialize(object)
    @object = object
    @resource = object[:resource]
    @id = object[:resource].id
    @email = object[:resource].email
    @auth_token = object[:resource].auth_token
  end

  def profile
    return {} if resource.profile_only_fetch_auth.nil?
    if resource.profile_only_fetch_auth.is_a?(User::Profile)
      User::ProfileAuthSerializer.new(resource.profile_only_fetch_auth).as_json
    else
      Partner::ProfileAuthSerializer.new(resource.profile_only_fetch_auth).as_json
    end
  end

  def links
    base =
      if resource.is_a?(User)
        {
          profile: profile_user_path(resource)
        }
      else
        {
          profile: profile_partner_path(resource),
          working_times: list_working_times_partner_path(resource),
          today_times: today_working_times_partner_path(resource),
          coming_soon_orders: coming_soon_orders_path,
          history_orders: history_orders_path,
          scheduled_orders: scheduled_orders_path,
          earning_reports: earning_reports_path
        }
      end

    object[:links] ? object[:links].merge(base) : base
  end
end
