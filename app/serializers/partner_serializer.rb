class PartnerSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  has_one :profile
  has_many :featured_images
  has_many :services do
    object.services.select(Partner::Service.base_columns)
  end
  has_many :albums do
    object.albums.select(Partner::Album.base_columns).limit(3)
  end

  attributes :id, :email, :reviews, :links

  def reviews
    rvs = object.reviews.includes(:user_profile).select(Partner::Review.base_columns).limit(5)
    ActiveModelSerializers::SerializableResource.new(rvs, each_serializer: Partner::ReviewSerializer)
  end

  def links
    {
      self: profile_partner_path(object),
      albums: list_albums_partner_path(object),
      reviews: list_reviews_partner_path(object),
      update_services: update_services_partner_path(object)
    }
  end
end
