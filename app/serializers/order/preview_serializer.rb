class Order
  class PreviewSerializer < ActiveModel::Serializer
    attributes :id, :location, :address, :status
  end
end
