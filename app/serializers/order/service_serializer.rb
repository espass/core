class Order
  class ServiceSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes :partner_service_id, :price
  end
end
