class Order
  class NotificationSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes [*Order.notification_columns, :user, :partner, :links]

    def user
      return {} if object.user_profile.nil?
      User::ProfilePreviewSerializer.new(object.user_profile).as_json
    end

    def partner
      return {} if object.partner_profile.nil?
      Partner::ProfilePreviewSerializer.new(object.partner_profile).as_json
    end

    def links
      related_links = {}
      case object.status
      when Constants::OrderStatus::SUCCESS
        related_links[:create_review] = create_review_partner_path(id: object.partner_id)
      when Constants::OrderStatus::REQUESTED
        related_links[:accept] = accept_order_path(object)
        related_links[:decline] = decline_order_path(object)
      when Constants::OrderStatus::PENDING
        related_links[:cancel] = cancel_order_path(object)
      end
      related_links
    end
  end
end
