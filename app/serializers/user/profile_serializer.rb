class User
  class ProfileSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes User::Profile.base_columns

    def avatar
      object.avatar.present? ? object.avatar : ApplicationHelper.data_default[:avatar]
    end

    def cover_photo
      object.cover_photo.present? ? object.cover_photo : ApplicationHelper.data_default[:cover_photo]
    end
  end
end
