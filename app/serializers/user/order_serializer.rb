class User
  class OrderSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes [*Order.base_columns, :partner, :links]

    def partner
      return {} if object.partner_profile.nil?
      Partner::ProfileSerializer.new(object.partner_profile).as_json
    end

    def links
      links = {
        detail: detail_order_path(object),
        cancel: cancel_order_path(object),
        finish: finish_order_path(object)
      }
      links[:profile] = profile_partner_path(id: object.partner_id) if object.partner_id.present?
      links
    end
  end
end
