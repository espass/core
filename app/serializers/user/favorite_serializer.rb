class User
  class FavoriteSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes :id, :created_at, :updated_at, :links

    def links
      {
        self: favorite_partner_users_path,
        my_partners: my_partners_users_path
      }
    end
  end
end
