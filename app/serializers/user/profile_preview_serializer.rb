class User
  class ProfilePreviewSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes User::Profile.preview_columns

    def avatar
      object.avatar.present? ? object.avatar : ApplicationHelper.data_default[:avatar]
    end
  end
end
