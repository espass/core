class User
  class SettingSerializer < ActiveModel::Serializer
    attributes :support, :slide_images, :services, :issue

    def support
      {
        hotline: I18n.t('resources.setting.user.support.hotline')
      }
    end

    def slide_images
      [
        {
          title: I18n.t('resources.setting.slides.slide1'),
          link: 'http://d2xadutqs8mj19.cloudfront.net/slides/slide1.jpg'
        },
        {
          title: I18n.t('resources.setting.slides.slide2'),
          link: 'http://d2xadutqs8mj19.cloudfront.net/slides/slide2.jpg'
        },
        {
          title: I18n.t('resources.setting.slides.slide3'),
          link: 'http://d2xadutqs8mj19.cloudfront.net/slides/slide3.jpg'
        }
      ]
    end

    def services
      ActiveModelSerializers::SerializableResource.new(
        BaseService.all, each_serializer: BaseServiceSerializer
      )
    end

    def issue
      issue_types = Constants.IssueTypeValues.map do |value|
        { value: value, text: I18n.t("resources.setting.user.issue_type.#{value}") }
      end

      {
        issue_types: issue_types
      }
    end
  end
end
