class User
  class ProfileAuthSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes User::Profile.auth_columns

    def cover_photo
      object.cover_photo.present? ? object.cover_photo : ApplicationHelper.data_default[:cover_photo]
    end
  end
end
