class ListNotificationSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :notifications, :pagination, :links

  def notifications
    ActiveModelSerializers::SerializableResource.new(
      object[:notifications], each_serializer: NotificationSerializer
    )
  end

  def pagination
    object[:pagination]
  end

  def links
    paging = object[:pagination]
    links = { self: list_notifications_path, next: nil, previous: nil }
    links[:next] = "#{links[:self]}?p=#{paging[:next_page]}" if paging[:next_page].present?
    links[:previous] = "#{links[:self]}?p=#{paging[:prev_page]}" if paging[:prev_page].present?
    links
  end
end
