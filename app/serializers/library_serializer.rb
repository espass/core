class LibrarySerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :url, :links

  def links
    {
      create: create_libraries_path,
      delete: delete_library_path(object)
    }
  end
end
