class BaseServiceSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attributes BaseService.base_columns
end
