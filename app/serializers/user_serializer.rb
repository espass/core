class UserSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  has_one :profile
  attributes :id, :email, :links

  def links
    {
      self: profile_user_path(object)
    }
  end
end
