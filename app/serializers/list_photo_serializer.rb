class ListPhotoSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :created_at, :title, :description, :photos, :links

  def photos
    ActiveModelSerializers::SerializableResource.new(
      object.photos.select(Partner::Photo.base_columns).limit(10),
      each_serializer: Partner::PhotoSerializer
    )
  end

  def links
    {
      self: album_photos_partner_path(id: object[:partner_id], ab_id: object[:id]),
      add_photos: add_album_photos_partner_path(id: object[:partner_id], ab_id: object[:id]),
      delete: delete_album_partner_path(id: object[:partner_id], ab_id: object[:id]),
      delete_photos: delete_album_photos_partner_path(id: object[:partner_id], ab_id: object[:id])
    }
  end
end
