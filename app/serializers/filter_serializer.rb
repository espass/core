class FilterSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attr_reader :object

  attributes :partners, :links

  def initialize(object)
    @object = object
  end

  def partners
    object.map do |partner|
      profile = partner.profile_only_fetch_preview
      next if profile.nil?

      Partner::ProfilePreviewSerializer.new(profile).as_json
    end
  end

  def links
    {
      self: filter_partners_path
    }
  end
end
