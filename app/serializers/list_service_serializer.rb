class ListServiceSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attr_reader :object
  attributes :services, :links

  def initialize(object)
    @object = object
  end

  def services
    ActiveModelSerializers::SerializableResource.new(object[:services], each_serializer: BaseServiceSerializer)
  end

  def links
    {
      self: list_services_path
    }
  end
end
