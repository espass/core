class EarningSerializer < ActiveModel::Serializer
  attr_reader :total_payout, :period_of_time
  attributes :period_of_time, :detail, :overview, :total_payout

  def initialize(orders, period_of_time)
    @object = orders
    @period_of_time = period_of_time
  end

  # Overview earnings by period of time
  def overview
    @detail_by_days.nil? ? {} : load_overview_data
  end

  def load_overview_data
    revenue = @detail_by_days.map { |d| d[:revenue] }.reduce(:+)
    edela_fee = @detail_by_days.map { |d| d[:edela_fee] }.reduce(:+)
    num_order = @detail_by_days.map { |d| d[:num_order] }.reduce(:+)
    @total_payout = @detail_by_days.map { |d| d[:payment] }.reduce(:+)

    {
      revenue: revenue,
      num_order: num_order,
      edela_fee: edela_fee
    }
  end

  # Load detail earning by day
  def detail
    @detail_by_days = []
    order_groups = object.group_by do |o|
      o.finished_at.strftime('%d-%m-%Y')
    end

    order_groups.each do |day|
      num_order = day[1].count
      order_revenue = day[1].map(&:price).compact
      revenue = order_revenue.count > 0 ? order_revenue.reduce(:+) : 0
      edela_fee = revenue * 0.1
      payment = revenue - edela_fee

      @detail_by_days << detail_by_day(day[0], num_order, revenue, edela_fee, payment)
    end
    @detail_by_days
  end

  def detail_by_day(day, num_order, revenue, edela_fee, payment)
    {
      day: day[0],
      num_order: num_order,
      revenue: revenue,
      edela_fee: edela_fee,
      payment: payment
    }
  end
end
