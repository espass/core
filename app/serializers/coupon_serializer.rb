class CouponSerializer < ActiveModel::Serializer
  attributes %w[code discount discount_type]
end
