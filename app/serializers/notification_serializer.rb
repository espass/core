class NotificationSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes [*Notification.base_fields, :unread, :links]

  def target
    case object.target_type
    when Constants::NotificationType::ORDER
      detail_order_path(id: object.target)
    end
  end

  def unread
    return false if DataCenter.current_resource.nil?
    DataCenter.current_resource.unread_notification?(object.id)
  end

  def links
    {
      delete: delete_notification_path(object),
      mark_read: mark_read_notification_path(object)
    }
  end
end
