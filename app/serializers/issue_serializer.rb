class IssueSerializer < ActiveModel::Serializer
  attributes Issue.base_columns
end
