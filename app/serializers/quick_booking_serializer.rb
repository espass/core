class QuickBookingSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes [*Order.quick_booking_columns, :user, :links]

  def initialize(object)
    @object = object
  end

  def user
    profile = object.user_profile
    profile.nil? ? nil : User::ProfileSerializer.new(profile).as_json
  end

  def links
    {
      self: create_quickly_orders_path,
      detail: detail_order_path(object)
    }
  end
end
