class Partner
  class ListServiceSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes :services

    def services
      ActiveModelSerializers::SerializableResource.new(object[:services], each_serializer: ServiceSerializer)
    end
  end
end
