class Partner
  class ProfileSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes [*Partner::Profile.base_columns, :favorited]

    def favorited
      return false if DataCenter.current_user.nil?
      DataCenter.current_user.favorited?(object.partner_id)
    end

    def avatar
      object.avatar.present? ? object.avatar : ApplicationHelper.data_default[:avatar]
    end
  end
end
