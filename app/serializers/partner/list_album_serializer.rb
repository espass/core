class Partner
  class ListAlbumSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes :albums, :links

    def albums
      ActiveModelSerializers::SerializableResource.new(
        object[:albums], each_serializer: Partner::AlbumSerializer
      )
    end

    def links
      {
        self: list_albums_partner_path(id: object[:partner][:id]),
        create: create_album_partner_path(id: object[:partner][:id])
      }
    end
  end
end
