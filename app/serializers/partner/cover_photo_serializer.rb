class Partner
  class CoverPhotoSerializer < ActiveModel::Serializer
    attributes Partner::CoverPhoto.base_columns
  end
end
