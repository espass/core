class Partner
  class PhotoSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes [*(Partner::Photo.base_columns - %w[partner_id]), :links]

    def links
      links = {}
      links[:create_cover_photo] = create_cover_photo_partner_path(id: object.partner_id)

      links
    end
  end
end
