class Partner
  class WorkingTimeSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes [*Partner::WorkingTime.base_columns, :links]

    def links
      {
        update: update_working_time_partner_path(id: object.partner_id, wt_id: object.id),
        delete: delete_working_time_partner_path(id: object.partner_id, wt_id: object.id)
      }
    end
  end
end
