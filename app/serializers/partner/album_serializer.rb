class Partner
  class AlbumSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes [*(Partner::Album.base_columns - %w[id partner_id]), :links]

    def thumbnail
      object.thumbnail.present? ? object.thumbnail : ApplicationHelper.data_default[:album]
    end

    def links
      {
        photos: album_photos_partner_path(id: object[:partner_id], ab_id: object[:id])
      }
    end
  end
end
