class Partner
  class RatingReportSerializer < ActiveModel::Serializer
    attributes RatingReport.base_columns
  end
end
