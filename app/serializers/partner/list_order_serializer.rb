class Partner
  class ListOrderSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes :orders, :pagination, :links

    def orders
      ActiveModelSerializers::SerializableResource.new(
        object[:orders], each_serializer: Partner::OrderSerializer
      )
    end

    def pagination
      object[:pagination]
    end

    def links
      return {} if object[:links].nil?
      paging = object[:pagination]
      links = { self: object[:links][:self], next: nil, previous: nil }

      links[:next] = "#{links[:self]}?p=#{paging[:next_page]}" if paging[:next_page].present?
      links[:previous] = "#{links[:self]}?p=#{paging[:prev_page]}" if paging[:prev_page].present?

      links
    end
  end
end
