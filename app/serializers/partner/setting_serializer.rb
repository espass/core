class Partner
  class SettingSerializer < ActiveModel::Serializer
    attributes :support, :services, :issue

    def support
      {
        hotline: I18n.t('resources.setting.partner.support.hotline')
      }
    end

    def services
      ActiveModelSerializers::SerializableResource.new(
        BaseService.all, each_serializer: BaseServiceSerializer
      )
    end

    def issue
      issue_types = Constants.IssueTypeValues.map do |value|
        { value: value, text: I18n.t("resources.setting.partner.issue_type.#{value}") }
      end

      {
        issue_types: issue_types
      }
    end
  end
end
