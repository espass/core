class Partner
  class ProfilePreviewSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes [*Partner::Profile.preview_columns, :links]

    def avatar
      object.avatar.present? ? object.avatar : ApplicationHelper.data_default[:avatar]
    end

    def links
      {
        profile: profile_partner_path(id: object.partner_id)
      }
    end
  end
end
