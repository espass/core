class Partner
  class ListWorkingTimeSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes :working_times, :links

    def working_times
      ActiveModelSerializers::SerializableResource.new(
        object[:working_times],
        each_serializer: Partner::WorkingTimeSerializer
      )
    end

    def links
      {
        self: list_working_times_partner_path(id: object[:partner][:id]),
        create: create_working_time_partner_path(id: object[:partner][:id])
      }
    end
  end
end
