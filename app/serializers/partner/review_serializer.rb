class Partner
  class ReviewSerializer < ActiveModel::Serializer
    attributes [*Partner::Review.base_columns, :user]

    def user
      object.user_profile.nil? ? {} : User::ProfilePreviewSerializer.new(object.user_profile)
    end
  end
end
