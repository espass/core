class Partner
  class ServiceSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes Partner::Service.base_columns
  end
end
