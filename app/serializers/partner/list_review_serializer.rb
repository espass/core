class Partner
  class ListReviewSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes :rating_report, :pagination, :reviews, :links

    def rating_report
      RatingReportSerializer.new(object[:partner].load_rating_report)
    end

    def reviews
      ActiveModelSerializers::SerializableResource.new(object[:reviews], each_serializer: ReviewSerializer)
    end

    def pagination
      object[:pagination]
    end

    def links
      paging = object[:pagination]
      links = { self: list_reviews_partner_path(id: object[:partner][:id]), next: nil, previous: nil }
      links[:next] = "#{links[:self]}?p=#{paging[:next_page]}" if paging[:next_page].present?
      links[:previous] = "#{links[:self]}?p=#{paging[:prev_page]}" if paging[:prev_page].present?
      links
    end
  end
end
