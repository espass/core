class Partner
  class OrderSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers
    attributes [*Order.base_columns, :user, :links]

    def user
      return {} if object.user_profile.nil?
      User::ProfileSerializer.new(object.user_profile).as_json
    end

    def links
      {
        detail: detail_order_path(object),
        cancel: cancel_order_path(object),
        process: process_order_path(object),
        finish: finish_order_path(object)
      }
    end
  end
end
