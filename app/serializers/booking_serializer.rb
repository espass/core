class BookingSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes [*Order.booking_columns, :partner, :user, :links]

  def initialize(object)
    @object = object
  end

  def partner
    profile = object.partner_profile
    profile.nil? ? nil : Partner::ProfileSerializer.new(profile).as_json
  end

  def user
    profile = object.user_profile
    profile.nil? ? nil : User::ProfileSerializer.new(profile).as_json
  end

  def links
    {
      self: create_orders_path,
      detail: detail_order_path(object)
    }
  end
end
