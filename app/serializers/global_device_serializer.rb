class GlobalDeviceSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :dios, :device_token, :links

  def dios
    object.id
  end

  def links
    { self: register_devices_path }
  end
end
